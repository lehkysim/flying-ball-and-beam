%% BALL AND BEAM

syms theta(t) d(t) tau(t)
syms d1 m1 r1 b1 I1 m2 r2 b2 I2 g

% first derivatives
Dtheta(t) = diff(theta(t),t);
Dd(t) = diff(d(t),t);

phi = d(t)/r2 + theta(t);
Dphi = diff(phi,t);

%% ENERGIES & LAGRANGIAN

% kinetic co-energy
T = 1/2*I1*Dtheta(t)^2 + 1/2*m2*(d(t)^2*Dtheta(t)^2+Dd(t)^2) + ...
    1/2*I2*(Dtheta(t)+Dphi)^2; 

% potential energy
h = d(t)*sin(theta(t)) + (r1+r2)*cos(theta(t));
V = m2*g*h; 

% Lagrangian
L = T - V;

% dissipation function
D = 1/2*b1*Dtheta(t)^2 + 1/2*b2*Dphi^2;

% conversion
Ls = subs(L,{Dtheta,theta,Dd,d}, ...
            {'Dtheta','theta','Dd','d'});
Ds = subs(D,{Dtheta,theta,Dd,d}, ...
            {'Dtheta','theta','Dd','d'});

%% PARTIAL DERIVATIVES

% dL/d Dtheta(t)
dLdDtheta = diff(Ls,'Dtheta');
% dL/d theta(t)
dLdtheta = diff(Ls,'theta');

% dL/d Dd(t)
dLdDd = diff(Ls,'Dd');
% dL/d d(t)
dLdd = diff(Ls,'d');

% dD/d Dtheta(t)
dDdDtheta = diff(Ds,'Dtheta');
% dD/d Dphi(t)
dDdDphi = diff(Ds,'Dd');

% reverse conversion
dLdDtheta = subs(dLdDtheta,{'Dtheta','theta','Dd','d'}, ...
                           {Dtheta,theta,Dd,d});
dLdtheta  = subs(dLdtheta,{'Dtheta','theta','Dd','d'}, ...
                          {Dtheta,theta,Dd,d});
dLdDd     = subs(dLdDd,{'Dtheta','theta','Dd','d'}, ...
                       {Dtheta,theta,Dd,d});
dLdd      = subs(dLdd,{'Dtheta','theta','Dd','d'}, ...
                      {Dtheta,theta,Dd,d});

dDdDtheta = subs(dDdDtheta,{'Dtheta','theta','Dd','d'}, ...
                           {Dtheta,theta,Dd,d});
dDdDphi = subs(dDdDphi,{'Dtheta','theta','Dd','d'}, ...
                           {Dtheta,theta,Dd,d});

% total time derivatives
ddLdDtheta = diff(dLdDtheta,'t');
ddLdDd = diff(dLdDd,'t');

%% LAGRANGE EQUATIONS

eqntheta = ddLdDtheta - dLdtheta + dDdDtheta - tau;  
eqnd = ddLdDd - dLdd + dDdDphi;

%% MODIFY LAGRANGE EQUATIONS 

eqntheta = simplify(collect(eqntheta,[diff(Dtheta,t), diff(Dd,t), Dtheta, Dd, theta, d]));
eqnd = simplify(collect(eqnd,[diff(Dtheta,t), diff(Dd,t), Dtheta, Dd, theta, d]));

% pretty(eqntheta)
% pretty(eqnd)

%% MATRICES

% M(q)*[DDtheta; DDd] + C(q,Dq)*[Dtheta; Dd] + G(q) = Q*tau

M = [m2*d(t)^2+I1+4*I2,   2*I2/r2;
          2*I2/r2,      m2+I2/r2^2];

C = [ 2*m2*Dd(t)*d(t)+b1+b2,   b2/r2;
     b2/r2-m2*Dtheta(t)*d(t), b2/r2^2];

G = [m2*g*cos(theta(t))*d(t)-m2*g*(r1+r2)*sin(theta(t));
                    m2*g*sin(theta(t))                 ];

Q = [1; 0];

%% STATE-SPACE

% [DDtheta; DDd] = M\(-C*[Dtheta; Dd] - G + Q*tau)

A = simplify(-M\C);
g = simplify(-M\G);
B = simplify(M\Q);

Ath1 = A(1,1);
Ath2 = A(1,2);
gth  = g(1);
Bth  = B(1);
% DDtheta = Ath1*Dtheta + Ath2*Dd + gth + Bth*tau

Ad1 = A(2,1);
Ad2 = A(2,2);
gd  = g(2);
Bd  = B(2);
% DDd = Ad1*Dtheta + Ad2*Dd + gd + Bd*tau
