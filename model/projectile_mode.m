%% PROJECTILE MOTION

syms x(t) y(t) phi(t)
syms d1 m1 r1 b1 I1 m2 r2 b2 I2 g

% first derivatives 
Dx(t) = diff(x(t),t);
Dy(t) = diff(y(t),t);
Dphi(t) = diff(phi(t),t);

%% ENERGIES & LAGRANGIAN

% kinetic energy
T = 1/2*m2*(Dx(t)^2 + Dy(t)^2) + 1/2*I2*Dphi(t)^2;

% potential energy
V = m2*g*y(t);

% Lagrangian
L = T - V;

% dissipation function
D = 0;

% conversion
Ls = subs(L,{Dx,x,Dy,y,Dphi,phi},{'Dx','x','Dy','y','Dphi','phi'});
Ds = subs(D,{Dx,x,Dy,y,Dphi,phi},{'Dx','x','Dy','y','Dphi','phi'});

%% PARTIAL DERIVATIVES

% dL/d Dx(t)
dLdDx = diff(Ls,'Dx');
% dL/d x(t)
dLdx = diff(Ls,'x');

% dL/d Dy(t)
dLdDy = diff(Ls,'Dy');
% dL/d y(t)
dLdy = diff(Ls,'y');

% dL/d Dphi(t)
dLdDphi = diff(Ls,'Dphi');
% dL/d phi(t)
dLdphi = diff(Ls,'phi');

% dD/d
dD = 0;

% reverse conversion
dLdDx     = subs(dLdDx,{'Dx','x','Dy','y','Dphi','phi'}, ...
                       {Dx,x,Dy,y,Dphi,phi});
dLdx      = subs(dLdx,{'Dx','x','Dy','y','Dphi','phi'}, ...
                      {Dx,x,Dy,y,Dphi,phi});
dLdDy     = subs(dLdDy,{'Dx','x','Dy','y','Dphi','phi'}, ...
                       {Dx,x,Dy,y,Dphi,phi});
dLdy      = subs(dLdy,{'Dx','x','Dy','y','Dphi','phi'}, ...
                       {Dx,x,Dy,y,Dphi,phi});
dLdDphi = subs(dLdDphi,{'Dx','x','Dy','y','Dphi','phi'}, ...
                       {Dx,x,Dy,y,Dphi,phi});
dLdphi  = subs(dLdphi,{'Dx','x','Dy','y','Dphi','phi'}, ...
                      {Dx,x,Dy,y,Dphi,phi});

% total time derivatives
ddLdDx = diff(dLdDx,'t');
ddLdDy = diff(dLdDy,'t');
ddLdDphi = diff(dLdDphi,'t');

%% LAGRANGE EQUATIONS

eqnx = ddLdDx - dLdx;
eqny = ddLdDy - dLdy;
eqnphi = ddLdDphi - dLdphi;

%% PRETTY LAGRANGE EQUATIONS

eqnx = simplify (collect(eqnx,[diff(Dx,t), diff(Dy,t), diff(Dphi,t), Dx, Dy, Dphi, x, y, phi]));
eqny = simplify(collect(eqny,[diff(Dx,t), diff(Dy,t), diff(Dphi,t), Dx, Dy, Dphi, x, y, phi]));
eqnphi = simplify(collect(eqnphi,[diff(Dx,t), diff(Dy,t), diff(Dphi,t), Dx, Dy, Dphi, x, y, phi]));

% pretty(eqnx)
% pretty(eqny)
% pretty(eqnphi)

%% STATE-SPACE

% m2*DDx = 0            =>  DDx = 0               
% m2*(DDy+g) = 0        =>  DDy = -g
% I2*DDphi = 0          =>  DDphi = 0
