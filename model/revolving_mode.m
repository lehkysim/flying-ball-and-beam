%% REVOLVING BALL ON BEAM

syms theta(t) alpha(t) tau(t)
syms d1 m1 r1 b1 I1 m2 r2 b2 I2 g

% first derivatives
Dtheta(t) = diff(theta(t),t);
Dalpha(t) = diff(alpha(t),t);

x1 = d1/2*cos(theta(t));
y1 = d1/2*sin(theta(t));
x2 = x1 + (r1+r2)*cos(theta(t)+alpha(t));
y2 = y1 + (r1+r2)*sin(theta(t)+alpha(t));

Dx2 = diff(x2,t);
Dy2 = diff(y2,t);

phi = theta(t) + r1/r2*alpha(t);
Dphi = diff(phi,t);

%% ENERGIES & LAGRANGIAN

% kinetic energy 
T = 1/2*I1*Dtheta(t)^2 + 1/2*m2*(Dx2^2+Dy2^2) + 1/2*I2*(Dtheta(t)+Dphi)^2;

% potential energy
V = m2*g*y2;

% Lagrangian
L = T - V;

% dissipation function
D = 1/2*b1*Dtheta(t)^2 + 1/2*b2*Dphi^2;

% conversion
Ls = subs(L,{Dtheta,theta,Dalpha,alpha}, ...
            {'Dtheta','theta','Dalpha','alpha'});
Ds = subs(D,{Dtheta,theta,Dalpha,alpha}, ...
            {'Dtheta','theta','Dalpha','alpha'});

%% PARTIAL DERIVATIVES

% dL/d Dtheta(t)
dLdDtheta = diff(Ls,'Dtheta');
% dL/d theta(t)
dLdtheta = diff(Ls,'theta');

% dL/d Dalpha(t)
dLdDalpha = diff(Ls,'Dalpha');
% dL/d alpha(t)
dLdalpha = diff(Ls,'alpha');

% dD/d Dtheta(t)
dDdDtheta = diff(Ds,'Dtheta');
% dD/d Dalpha(t)
dDdDalpha = diff(Ds,'Dalpha');

% reverse conversion
dLdDtheta = subs(dLdDtheta,{'Dtheta','theta','Dalpha','alpha'}, ...
                           {Dtheta,theta,Dalpha,alpha});
dLdtheta  = subs(dLdtheta,{'Dtheta','theta','Dalpha','alpha'}, ...
                          {Dtheta,theta,Dalpha,alpha});
dLdDalpha = subs(dLdDalpha,{'Dtheta','theta','Dalpha','alpha'}, ...
                           {Dtheta,theta,Dalpha,alpha});
dLdalpha  = subs(dLdalpha,{'Dtheta','theta','Dalpha','alpha'}, ...
                          {Dtheta,theta,Dalpha,alpha});

dDdDtheta = subs(dDdDtheta,{'Dtheta','theta','Dalpha','alpha'}, ...
                           {Dtheta,theta,Dalpha,alpha});
dDdDalpha = subs(dDdDalpha,{'Dtheta','theta','Dalpha','alpha'}, ...
                           {Dtheta,theta,Dalpha,alpha});

% total time derivatives
ddLdDtheta = diff(dLdDtheta,'t');
ddLdDalpha = diff(dLdDalpha,'t');

%% LAGRANGE EQUATIONS

eqntheta = ddLdDtheta - dLdtheta + dDdDtheta - tau;
eqnalpha = ddLdDalpha - dLdalpha + dDdDalpha;

%% MODIFY LAGRANGE EQUATIONS 

eqntheta = simplify(collect(eqntheta,[diff(Dtheta,t), diff(Dalpha,t), Dtheta, Dalpha, theta, alpha]));
eqnalpha = simplify(collect(eqnalpha,[diff(Dtheta,t), diff(Dalpha,t), Dtheta, Dalpha, theta, alpha]));

% pretty(eqntheta)
% pretty(eqnalpha)

%% MATRICES

% M(q)*[DDtheta; DDalpha] + C(d,Dq)*[Dtheta; Dalpha] + G(q) = Q*tau

m11 = I1+4*I2+m2*(r1+r2)^2+d1^2/4*m2+(r1+r2)*d1*m2*cos(alpha(t));
m12 = 2*I2*r1/r2+m2*(r1+r2)^2+(r1+r2)*d1/2*m2*cos(alpha(t)); 
m21 = m2*(r1+r2)^2+2*I2*r1/r2+(r1+r2)*d1/2*m2*cos(alpha(t));
m22 = I2*r1^2/r2^2+m2*(r1+r2)^2;

M = [m11, m12;
     m21, m22];

c11 = b1+b2-(r1+r2)*d1*m2*sin(alpha(t))*diff(alpha(t),t);
c12 = b2*r1/r2-(r1+r2)*d1/2*m2*sin(alpha(t))*diff(alpha(t),t);
c21 = b2*r1/r2+(r1+r2)*d1/2*m2*sin(alpha(t))*diff(theta(t),t);
c22 = b2*r1^2/r2^2;

C = [c11, c12
     c21, c22];

G = [d1/2*g*m2*cos(theta(t))+(r1+r2)*g*m2*cos(alpha(t)+theta(t)); 
                (r1+r2)*g*m2*cos(alpha(t)+theta(t))             ];

Q = [1; 0];

%% STATE-SPACE

% [DDtheta; DDalpha] = M\(-C*[Dtheta; Dalpha] - G + Q*tau)

A = simplify(-M\C);
g = simplify(-M\G);
B = simplify(M\Q);

Ath1 = A(1,1);
Ath2 = A(1,2);
gth  = g(1);
Bth  = B(1);
% DDtheta = Ath1*Dtheta + Ath2*Dalpha + gth + Bth*tau

Aalp1 = A(2,1);
Aalp2 = A(2,2);
galp  = g(2);
Balp  = B(2);
% DDalpha = Aalp1*Dtheta + Aalp2*Dalpha + galp + Balp*tau
