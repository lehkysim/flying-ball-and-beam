%% CREATE MODEL

syms theta Dtheta alpha Dalpha
syms tau
syms d1 m1 r1 b1 I1 m2 r2 b2 I2 g

x1 = d1/2*cos(theta);
y1 = d1/2*sin(theta);
x2 = x1 + (r1+r2)*cos(theta+alpha);
y2 = y1 + (r1+r2)*sin(theta+alpha);

Dx2 = -sin(alpha+theta)*(r1+r2)*(Dalpha+Dtheta) - 1/2*d1*sin(theta)*Dtheta;
Dy2 =  cos(alpha+theta)*(r1+r2)*(Dalpha+Dtheta) + 1/2*d1*cos(theta)*Dtheta;

phi = theta + r1/r2*alpha;
Dphi = Dtheta + r1/r2*Dalpha;

% kinetic energy
T = 1/2*I1*Dtheta^2 + 1/2*m2*(Dx2^2+Dy2^2) + 1/2*I2*(Dtheta+Dphi)^2;

% potential energy
V = m2*g*y2;

% Lagrangian
L = T - V;
X = {theta Dtheta alpha Dalpha};
Q_i = {0 0};
Q_e = {tau 0};

% dissipation function
R = 1/2*b1*Dtheta^2 + 1/2*b2*Dphi^2;

par = {d1 m1 r1 b1 I1 m2 r2 b2 I2 g};

% VF = EulerLagrange(L,X,Q_i,Q_e,R,par,'m','Revolving_sys');
