%% SIMULATE
close all;

params;

tspan = [0 5];
Y0 = [pi/2 0 0 0];
options = odeset('RelTol',1e-8);

% [t1, Y1] = ode45(@Revolving_sys,tspan,Y0,options);
[t2, Y2] = ode45(@Revolving_enum,tspan,Y0,options);

%% BALL ANGLE AND ANGULAR VELOCITY

f = figure;
f.Position = [100 100 700 250];

plot(t2,Y2(:,3),'LineWidth',2);
hold on;
plot(t2,Y2(:,4),'LineWidth',2,'Color','#A2142F');
hold off;
grid on;
xlabel('Time [s]');
ylabel('{\alpha} [rad], D{\alpha}(t) [rad/s]');
xlim(tspan);
% ylim([-2 2]);
legend('Ball angle','Ball angular velocity','Location','SouthWest');
set(gca,'FontSize',15);

%% BEAM ANGLE AND ANGULAR VELOCITY

f = figure;
f.Position = [100 100 700 250];

plot(t2,Y2(:,1),'LineWidth',2);
hold on;
plot(t2,Y2(:,2),'LineWidth',2,'Color','#A2142F');
hold off;
grid on;
xlabel('Time [s]');
ylabel('{\theta} [rad], D{\theta} [rad/s]');
xlim(tspan);
% ylim([-2 2]);
legend('Beam angle','Beam angular velocity','Location','SouthWest');
set(gca,'FontSize',15);
