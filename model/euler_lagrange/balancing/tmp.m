%% VARIABLES

syms theta Dtheta d Dd
syms tau
Y = [theta Dtheta d Dd];

d1 = prms.d1;
m1 = prms.m1;
r1 = prms.r1;
b1 = prms.b1;
I1 = prms.I1;
m2 = prms.m2;
r2 = prms.r2;
b2 = prms.b2;
I2 = prms.I2;
g  = prms.g;

%% ENUMERATE VECTOR

t2 = Y(1);
t3 = Y(2);
t4 = Y(3);
t5 = Y(4);
t6 = m2.^2;
t7 = r2.^2;
t8 = r2.^3;
t9 = I1.*I2;
t10 = cos(t2);
t11 = sin(t2);
t12 = t3.^2;
t13 = t4.^2;
t14 = I1.*m2.*t7;
t15 = I2.*m2.*t7.*4.0;
t16 = I2.*m2.*t13;
t17 = t6.*t7.*t13;
t18 = t9+t14+t15+t16+t17;
t19 = 1.0./t18;
mt1 = [t3;-(t19.*(-I2.*b2.*t5-I2.*r2.*tau-m2.*t8.*tau+b1.*m2.*t3.*t8+b2.*m2.*t3.*t8+b2.*m2.*t5.*t7-g.*t6.*t7.^2.*t11+I2.*b1.*r2.*t3-I2.*b2.*r2.*t3-I2.*g.*m2.*t7.*t11.*3.0+I2.*m2.*t4.*t7.*t12.*2.0-g.*r1.*t6.*t8.*t11+g.*t4.*t6.*t8.*t10+t3.*t4.*t5.*t6.*t8.*2.0-I2.*g.*m2.*r1.*r2.*t11+I2.*g.*m2.*r2.*t4.*t10+I2.*m2.*r2.*t3.*t4.*t5.*2.0))./r2;t5];
mt2 = [-t19.*(I1.*b2.*t5+I2.*b2.*t5.*2.0+I2.*r2.*tau.*2.0+g.*t11.*t14+g.*t11.*t17-t4.*t12.*t14+b2.*m2.*t5.*t13-t4.^3.*t6.*t7.*t12+I1.*b2.*r2.*t3-I2.*b1.*r2.*t3.*2.0+I2.*b2.*r2.*t3.*2.0+I2.*g.*m2.*t7.*t11.*6.0-I2.*m2.*t4.*t7.*t12.*4.0+b2.*m2.*r2.*t3.*t13+I2.*g.*m2.*r1.*r2.*t11.*2.0-I2.*g.*m2.*r2.*t4.*t10.*2.0-I2.*m2.*r2.*t3.*t4.*t5.*4.0)];
dYdt = [mt1;mt2];

%% STATE VECTOR FOR BALANCING MODE

dYdt = ...
    [Dtheta;
    -(100*(1.5151e-10*Dd + 1.9705e-11*Dtheta - 4.5780e-08*tau - 4.8395e-10*sin(theta) + 8.5543e-12*Dtheta^2*d + 1.4681e-08*d*cos(theta) + 2.9940e-09*Dd*Dtheta*d))/(1.4970e-07*d^2 + 4.0732e-09);
    Dd;
    -(6.8619e-08*Dd + 6.7580e-10*Dtheta + 2.6160e-08*tau + 2.8809e-08*sin(theta) + 1.0487e-06*d^2*sin(theta) + 2.5251e-06*Dd*d^2 + 2.5251e-08*Dtheta*d^2 - 2.9143e-09*Dtheta^2*d - 8.3892e-09*d*cos(theta) - 1.0693e-07*Dtheta^2*d^3 - 1.7109e-09*Dd*Dtheta*d)/(1.4970e-07*d^2 + 4.0732e-09)];
