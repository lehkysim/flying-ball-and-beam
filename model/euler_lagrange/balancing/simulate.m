%% SIMULATE
close all;

params;
load("data/real.mat");

time = compar.time;
angle = compar.signals(1).values;
angle = reshape(angle,[],1);
Dangle = compar.signals(2).values;
dist = compar.signals(3).values;
Ddist = compar.signals(4).values;

tspan = [0 0.4];
Y0 = [0 0 0.03 0];
options = odeset('RelTol',1e-8);
 
% [t1, Y1] = ode45(@Balancing_sys,tspan,Y0,options);
[t2, Y2] = ode45(@Balancing_enum,tspan,Y0,options);

%% BALL DISTANCE

beg = 30;
f = figure;
f.Position = [100 100 700 250];

plot(t2,Y2(:,3),'LineWidth',2);
hold on;
plot(time(beg:end)-beg*0.03,dist(beg:end)/1000,'LineWidth',2,'Color','#A2142F');
hold off;
grid on;
xlabel('Time [s]')
ylabel('Ball distance [m]');
xlim(tspan);
ylim([0.02 0.09]);
legend('Mathematical model','Real model','Location','NorthWest');
set(gca,'FontSize',15);

%% BALL VELOCITY

beg = 31;
f = figure;
f.Position = [100 100 700 250];

plot(t2,Y2(:,4),'LineWidth',2);
hold on;
plot(time(beg:end)-beg*0.03,Ddist(beg:end)/1000-0.015,'LineWidth',2,'Color','#A2142F');
hold off;
grid on;
xlabel('Time [s]')
ylabel('Ball velocity [m/s]');
xlim(tspan);
ylim([-0.1 0.4]);
legend('Mathematical model','Real model','Location','NorthWest');
set(gca,'FontSize',15);

%% BEAM ANGLE

beg = 30;
f = figure;
f.Position = [100 100 700 250];

plot(t2,Y2(:,1),'LineWidth',2);
hold on;
plot(time(beg:end)-beg*0.03,angle(beg:end)+0.07,'LineWidth',2,'Color','#A2142F');
hold off;
grid on;
xlabel('Time [s]')
ylabel('Beam angle [rad]');
xlim(tspan);
ylim([-1.1 0.1]);
legend('Mathematical model','Real model','Location','SouthWest');
set(gca,'FontSize',15);

%% BEAM VELOCITY

beg = 30;
f = figure;
f.Position = [100 100 700 250];

plot(t2,Y2(:,2),'LineWidth',2);
hold on;
plot(time(beg:end)-beg*0.03,Dangle(beg:end)+0.08,'LineWidth',2,'Color','#A2142F');
hold off;
grid on;
xlabel('Time [s]')
ylabel('Beam velocity [rad/s]');
xlim(tspan);
ylim([-6 1]);
legend('Mathematical model','Real model','Location','SouthWest');
set(gca,'FontSize',15);
