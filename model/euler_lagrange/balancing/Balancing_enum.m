function dYdt = Balancing_enum(t, Y)    %#ok

tau = 0;

theta = Y(1);
Dtheta = Y(2);
d = Y(3);
Dd = Y(4);

dYdt = ...
    [Dtheta;
    -(100*(1.5151e-10*Dd + 1.9705e-11*Dtheta - 4.5780e-08*tau - 4.8395e-10*sin(theta) + 8.5543e-12*Dtheta^2*d + 1.4681e-08*d*cos(theta) + 2.9940e-09*Dd*Dtheta*d))/(1.4970e-07*d^2 + 4.0732e-09);
    Dd;
    -(6.8619e-08*Dd + 6.7580e-10*Dtheta + 2.6160e-08*tau + 2.8809e-08*sin(theta) + 1.0487e-06*d^2*sin(theta) + 2.5251e-06*Dd*d^2 + 2.5251e-08*Dtheta*d^2 - 2.9143e-09*Dtheta^2*d - 8.3892e-09*d*cos(theta) - 1.0693e-07*Dtheta^2*d^3 - 1.7109e-09*Dd*Dtheta*d)/(1.4970e-07*d^2 + 4.0732e-09)];

end
