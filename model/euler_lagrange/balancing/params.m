clear prms
clear rpi

setenv('TMPDIR','/home/simon/project/');

%% PARAMETERS

prms.g = 9.807;         % gravitational acceleration
prms.Ts = 0.005;
% prms.Ts_cam = 0.03;

%% BALL

prms.m2 =  32.7e-3;     % mass
prms.d2 =  20.0e-3;     % diameter
prms.r2 = prms.d2/2;    % radius
prms.b2 = 2.574e-6*30;     % friction

%% BEAM

prms.m1 =  64.0e-3;     % mass
prms.l1 = 204.0e-3;     % length 
prms.w1 =  20.0e-3;     % width
prms.h1 =  34.5e-3;     % height
prms.r1 = prms.h1/2;    % radius of semi-circles
prms.d1 = 170.5e-3;     % length without semi-circles
prms.b1 = 15.893e-4/4;     % motor friction

% height when ball placed on beam
prms.height_tmp = 51.5e-3; 
% beam groove = 3 mm
prms.g1 = prms.d2 + prms.h1 - prms.height_tmp;

% offset between ball center and beam central axis
prms.h12 = prms.r1 + prms.r2 - prms.g1; 

%% MOMENTS OF INERTIA

% I1 = 1/12*prms.m1*prms.l1^2;
prms.I1 = 8.860e-4;

prms.I2 = 2/5*prms.m2*prms.r2^2;

%% CALIBRATION MATRIX

% addpath('./calib');
% load("H.mat");
% rmpath('./calib');

%% EQUATION COEFFS

% addpath('./m/eqn');
% load("coefs.mat");
% rmpath('./m/eqn');

%% OTHER 

prms.thrs = 0;  % TODO

%% RASPBERRY INFO

rpi.ip   = '192.168.0.101';
rpi.port = 5001;
rpi.user = 'pi';
rpi.pswd = 'simonrpi';
