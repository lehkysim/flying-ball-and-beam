%% CREATE MODEL

syms theta Dtheta d Dd
syms tau
syms d1 m1 r1 b1 I1 m2 r2 b2 I2 g

phi = d/r2 + theta;
Dphi = Dd/r2 + Dtheta;

% kinetic energy 
T = 1/2*I1*Dtheta^2 + 1/2*m2*(d^2*Dtheta^2+Dd^2) + ...
    1/2*I2*(Dtheta+Dphi)^2; 

% potential energy
h = d*sin(theta) + (r1+r2)*cos(theta);
V = m2*g*h;

% Lagrangian
L = T - V;
X = {theta Dtheta d Dd};
Q_i = {0 0};
Q_e = {tau 0};

% dissipation function  
R = 1/2*b1*Dtheta^2 + 1/2*b2*Dphi^2;

par = {d1 m1 r1 b1 I1 m2 r2 b2 I2 g};

VF = EulerLagrange(L,X,Q_i,Q_e,R,par,'m','Balancing_sys');
