%% CREATE MODEL

syms x Dx y Dy phi Dphi
syms d1 m1 r1 b1 I1 m2 r2 b2 I2 g

% kinetic energy
T = 1/2*m2*(Dx^2 + Dy^2) + 1/2*I2*Dphi^2;

% potential energy
V = m2*g*y;

% Lagrangian
L = T - V;
X = {x Dx y Dy phi Dphi};
Q_i = {0 0 0};
Q_e = {0 0 0};

% dissipation function
R = 0;

par = {d1 m1 r1 b1 I1 m2 r2 b2 I2 g};

% VF = EulerLagrange(L,X,Q_i,Q_e,R,par,'m','Projectile_sys');
