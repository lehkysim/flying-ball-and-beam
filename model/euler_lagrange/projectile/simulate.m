%% SIMULATE
close all;

params;

tau = 0;
tspan = [0 0.1428];
Y0 = [0 0 0.1 0 0 0];
options = odeset('RelTol',1e-8);

[t, Y] = ode45(@Projectile_sys,tspan,Y0,options,prms);

Y(:,4) = -Y(:,4); 

%% BALL Y POSITION

f = figure;
f.Position = [100 100 700 250];

plot(t,Y(:,3),'LineWidth',2);
grid on;
xlabel('Time [s]');
ylabel('Ball position y [m]');
xlim(tspan);
ylim([-0.02, 0.12]);
set(gca,'FontSize',15);
