# Flying Ball and Beam

This repository serves as a project record for my bachelor thesis written under the supervision of Ing. Jiří Zemánek, PhD. in the Department of Control Engineering at FEE CTU in Prague.

## Info
The thesis aims to utilize a laboratory model Flying Ball in Hoop designed at FEE CTU and adjust it to a ball and beam system. \
The goal is to create a hybrid system comprising three different modes: a balancing mode, a revolving mode, and a projectile mode. I will make a mathematical model for all three modes of the hybrid system, which will then be verified with a visualization. \
After that, I intend to implement a controller for balancing the ball on the beam and create an optimal trajectory for projecting the ball from one side of the beam to the other. The last step will be to create documentation for the laboratory model and prepare the system for use in lectures at FEE CTU. \
The paper can be seen at https://gitlab.fel.cvut.cz/lehkysim/fbab-thesis.

## Progress List

#### DONE
* Hardware modification
* Camera homography
* Motor friction identification
* Mathematical models of the hybrid system
    * Balancing mode
    * Revolving mode
    * Projectile mode
* Linearization of the modes
* Visualization of the ball and beam system
* State Flow diagram
* Guard conditions of the hybrid system
* Coords transposition
* Flat side balancing
* Curved side balancing (not possible)
* Review and completion of the system documentation

#### WIP
* Demonstration of the ball throw
