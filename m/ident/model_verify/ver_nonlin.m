N = 600;

[t, u] = create_sine(N, prms);
plot(t,u);

y_prev = zeros(4,1);
y_est = zeros(4,N);

for i = 1:(N-1)
    u_curr = u(i);
    [~, y] = my_rk4(bm_vec(t, y, u_curr, coefs), [0, prms.Ts], prms.Ts, y_prev);
%     [~,y] = ode45(@(t,y) bm_vec(t, y, u_curr, coefs), [0 prms.Ts], y_prev); 
    y_prev = y(end,:)';
    y_est(:,i+1) = y_prev;
end

% plot(t,y_est(1,:),t,y_est(2,:),t,y_est(3,:),t,y_est(4,:));
