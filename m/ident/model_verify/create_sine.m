function [t, u] = create_sine(N, prms)
    
    % create sine waveform
    A = 0.2;
    w = 3;
    
    t = prms.Ts*(0:N-1);
    u = A*sin(w*t);

end

