function [t, y] = my_rk4(F, tspan, h, x0)

    t = tspan(1):h:tspan(2);
    x = zeros(length(t),1);
    x(:) = x0;

    k1 = F(t,     x,        u_curr, coefs);
    k2 = F(t+h/2, x+h*k1/2*ones(numel(x),1), u_curr, coefs);
    k3 = F(t+h/2, x+h*k2/2*ones(numel(x),1), u_curr, coefs);
    k4 = F(t+h,   x+h*k3*ones(numel(x),1),   u_curr, coefs);

    y = x + (k1 + 2*k2 + 2*k3 + k4)*h/6;

end
