function dX = bm_vec(t, y, tau, coefs)

    % X = [theta; Dtheta; d; Dd]

    syms theta(t) d(t)
    Dtheta(t) = diff(theta(t),t);
    Dd(t) = diff(d(t),t);

    bmAth1 = subs(coefs.bmAth1,{theta,Dtheta,d,Dd},{y(1),y(2),y(3),y(4)});
    bmAth2 = subs(coefs.bmAth2,{theta,Dtheta,d,Dd},{y(1),y(2),y(3),y(4)});
    bmGth1 = subs(coefs.bmGth1,{theta,Dtheta,d,Dd},{y(1),y(2),y(3),y(4)});
    bmGth2 = subs(coefs.bmGth2,{theta,Dtheta,d,Dd},{y(1),y(2),y(3),y(4)});
    bmBth  = subs(coefs.bmBth, {theta,Dtheta,d,Dd},{y(1),y(2),y(3),y(4)});

    bmAd1 = subs(coefs.bmAd1,{theta,Dtheta,d,Dd},{y(1),y(2),y(3),y(4)});
    bmAd2 = subs(coefs.bmAd2,{theta,Dtheta,d,Dd},{y(1),y(2),y(3),y(4)});
    bmGd1 = subs(coefs.bmGd1,{theta,Dtheta,d,Dd},{y(1),y(2),y(3),y(4)});
    bmGd2 = subs(coefs.bmGd2,{theta,Dtheta,d,Dd},{y(1),y(2),y(3),y(4)});
    bmBd  = subs(coefs.bmBd, {theta,Dtheta,d,Dd},{y(1),y(2),y(3),y(4)});

    % DDtheta = coefs.bmAth1*Dtheta + coefs.bmAth2*Dd + coefs.bmGth1*sin(theta) + coefs.bmGth2*d + coefs.bmBth*tau
    % DDd = coefs.bmAd1*Dtheta + coefs.bmAd2*Dd + coefs.bmGd1*sin(theta) + coefs.bmGd2*d + coefs.bmBd*tau

    dX = [y(2);
          bmAth1*y(2) + bmAth2*y(4) + bmGth1*sin(y(1)) + bmGth2*y(3) + bmBth*tau;
          y(4);
          bmAd1*y(2) + bmAd2*y(4) + bmGd1*sin(y(1)) + bmGd2*y(3) + bmBd*tau];

    dX = double(dX);

end
