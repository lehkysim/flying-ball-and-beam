clear simin;

%% CHOOSE AMPLITUDE

amp = 0.65;
input = [0 amp 0];    % choose input

%% CREATE WAVEFORM

T_end = 10;
N = T_end/prms.Ts;

pwm = [80, 15];

time = prms.Ts*(0:N-1);
wav = get_input(input, prms.Ts, N, pwm);

simin.time = time';
simin.signals.values = wav';

%% PLOT WAVEFORM

plot(simin.time,simin.signals.values);
ylim([-1.6, 1.6]);
grid on;
