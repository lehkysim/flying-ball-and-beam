function wav = get_input(input, Ts, N, pwm)

    step = 10*Ts;
    
    % PWM
    alpha = pwm(1)*step;
    beta  = pwm(2)*step;
    
    count = 0;
    index = 1;
    nonzero = true;     % waveform starts with non-zero value
    
    wav = zeros(1,N);
    
    %% CREATE WAVEFORM
    
    for i = 1:N
    
        count = count + 1;
        if nonzero == true
            wav(i) = input(index);
            if count == alpha/Ts
                nonzero = false;
                index = index + 1;
                count = 0;
            end
        else
            wav(i) = 0;
            if count == beta/Ts
                nonzero = true;
                count = 0;
            end
        end
    
        if index == length(input)
            index = 1;
        end
    end
end
