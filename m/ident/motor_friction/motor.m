function dy = motor(t, y, u, I, b)    %#ok
    
    dy = -b/I*y + u;

end

