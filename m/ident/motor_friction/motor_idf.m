function [dy, out] = motor_idf(t, y, u, I, b, varargin)    %#ok
    
    dy = -b/I*y + u;
    out = y/I;

end

