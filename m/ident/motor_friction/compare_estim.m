%% MOTOR

Ts = 0.03;

load("estim_data.mat");

t = data.time;
u = data.signals(1).values;
ang = data.signals(2).values;
yout = data.signals(3).values;

plot(t, yout);

%% MODEL

inputs;
input = train1;    % choose input

N = 500;
T_end = Ts*N;
pwm = [1, 5];

time = Ts*(0:N-1);
wav = get_input(input, Ts, N, pwm);

y_prev = 0;
y_est = zeros(1,N);

I_est = 5.3160e-04;
b_est = 4.7678e-04;

for i = 1:(N-1)
    u_curr = wav(i);
    [~,y] = ode45(@(t,y) motor(t, y, u_curr, I_est, b_est), [0 Ts], y_prev); 
    y_prev = y(end);
    y_est(i+1) = y_prev;
end

plot(t, yout, time, y_est/I_est);
