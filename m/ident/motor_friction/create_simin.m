clear simin;

%% CHOOSE INPUT

inputs;
input = train1;    % choose input

%% CREATE WAVEFORM

N = 1000;
T_end = Ts*N;
pwm = [1, 5];

time = Ts*(0:N-1);
wav = get_input(input, Ts, N, pwm);

simin.time = time';
simin.signals.values = wav';

%% PLOT WAVEFORM

plot(simin.time,simin.signals.values);
ylim([-0.22, 0.22]);
grid on;
