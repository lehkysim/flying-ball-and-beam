Ts = 0.03;

%% CHOOSE INPUT

inputs;
input = eval2; 

%% PARAMS

% load("identified.mat");

% I_est = 5.3160e-04;
I_est = 5.3160e-04/0.6;
% b_est = 4.7678e-04
b_est = 4.7678e-04/0.3;

% estimation
I = 1/12*prms.m1*prms.l1^2;
b = 0.0001;

N = 2000;
T_end = Ts*N;
pwm = [1, 15];

time = Ts*(0:N-1);
u = get_input(input, Ts, N, pwm);

simin.time = time';
simin.signals.values = u';

%% PLOT INPUT

figure;
plot(time,u,'LineWidth',2);
ylim([-0.12, 0.12]);
grid on;

%% SIMULATE MOTOR MODEL

y_prev = 0;
y_est = zeros(1,N);

for i = 1:(N-1)
    u_curr = u(i);
    [~,y] = ode45(@(t,y) motor(t, y, u_curr, I_est, b_est), [0 Ts], y_prev); 
    y_prev = y(end);
    y_est(i+1) = y_prev;
end

save("eval_sim.mat","time","y_est");
