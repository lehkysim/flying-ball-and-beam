%% COMPARE MOTOR MODEL AND REAL MOTOR

load("eval_sim.mat");
load("eval_real.mat");
load("identified.mat");

t = data.time;
u = data.signals(1).values;
ang = data.signals(2).values;
vel = data.signals(3).values;

f = figure;
f.Position = [100 100 700 500];

plot(t,vel,'LineWidth',2.5,'Color','#007DC7');
hold on;
plot(time,y_est/I_est,'LineWidth',2.5,'Color','#A2142F');
hold off;
grid on;

xlim([0 28.5]);
ylim([-22 25]);
xlabel('Time [s]');
ylabel('Velocity [rad/s]');

legend('Real motor','Motor model','Location','NorthEast');
set(gca,'FontSize',15);
