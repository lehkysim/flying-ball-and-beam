
Ts = 0.03;

% estimation
I = 1/12*prms.m1*prms.l1^2;
b = 0.0001;

load("estim_data.mat");

t = data.time;
u = data.signals(1).values;
ang = data.signals(2).values;
yout = data.signals(3).values;

%% PLOT MEASURED DATA

figure;
plot(t,ang,t,yout,'LineWidth',2);
grid on;
legend('Angle [rad]','Velocity [rad/s]','Location','NorthWest','FontSize',12);

%% NONLINEAR GREY-BOX MODEL

model_data = iddata(yout,u,Ts,'Name','tmp');
% order = [out, in, states]
order = [1, 1, 1];
parameters = {I,b};
initial_states = 0;

model = idnlgrey('motor_idf',order,parameters,initial_states,0);

setpar(model,'Fixed',{false,false});
setpar(model,'Minimum',{0,0});

model = nlgreyest(model_data,model,'Display','Full');

I_est = model.Parameters(1).Value;
b_est = model.Parameters(2).Value;
save("identified.mat", "I_est", "b_est");

fprintf("Moment of inertia of the beam is: %s. The motor friction is %s.\n", I_est, b_est);
