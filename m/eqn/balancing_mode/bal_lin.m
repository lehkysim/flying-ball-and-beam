%% GET BM COEFFICIENTS

bmAth1 = coefs.bmAth1;
bmAth2 = coefs.bmAth2;
bmGth1 = coefs.bmGth1;
bmGth2 = coefs.bmGth2;
bmBth  = coefs.bmBth;

bmAd1 = coefs.bmAd1;
bmAd2 = coefs.bmAd2;
bmGd1 = coefs.bmGd1;
bmGd2 = coefs.bmGd2;
bmBd  = coefs.bmBd;

%% LINEARIZE

syms theta Dtheta d Dd tau

theta_dot = Dtheta;
d_dot = Dd;
Dtheta_dot = bmAth1*theta_dot + bmAth2*d_dot + bmGth1*sin(theta) + bmGth2*d + bmBth*tau;
Dd_dot = bmAd1*theta_dot + bmAd2*d_dot + bmGd1*sin(theta) + bmGd2*d + bmBd*tau;

f = [theta_dot; 
     Dtheta_dot; 
     d_dot; 
     Dd_dot];

% operating point
xp = [0; 0; 0; 0];
taup = 0;

% matrix A
A = jacobian(f, [theta, Dtheta, d, Dd]);
A = double(subs(A,{'theta','Dtheta','d','Dd'},{xp(1),xp(2),xp(3),xp(4)}));

% matrix B
B = diff(f, tau);
B = double(subs(B,{'theta','Dtheta','d','Dd','tau'},{xp(1),xp(2),xp(3),xp(4),taup}));

% matrix C
C = [1 0 0 0;
     0 0 0 0; 
     0 0 1 0; 
     0 0 0 0];

% matrix D
D = 0;

bm_sys = ss(A,B,C,D);
% discrete state-space
bm_sys_d = c2d(bm_sys,prms.Ts,'zoh');
A_d = bm_sys_d.A;
B_d = bm_sys_d.B;
C_d = bm_sys_d.C;
D_d = bm_sys_d.D;

%% LQR

Q = C_d'*C_d;
R = 1;

[K,S,P] = lqr(bm_sys_d, Q, R);
