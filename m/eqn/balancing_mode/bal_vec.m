function dX = bal_vec(X, tau, coefs)

    % X = [theta; Dtheta; d; Dd]
    theta  = X(1);
    Dtheta = X(2);
    d  = X(3);
    Dd = X(4);

    bmAth1 = coefs.bmAth1;
    bmAth2 = coefs.bmAth2;
    bmGth1 = coefs.bmGth1;
    bmGth2 = coefs.bmGth2;
    bmBth  = coefs.bmBth;

    bmAd1 = coefs.bmAd1;
    bmAd2 = coefs.bmAd2;
    bmGd1 = coefs.bmGd1;
    bmGd2 = coefs.bmGd2;
    bmBd  = coefs.bmBd;

    % DDtheta = coefs.bmAth1*Dtheta + coefs.bmAth2*Dd + coefs.bmGth1*sin(theta) + coefs.bmGth2*d + coefs.bmBth*tau
    % DDd = coefs.bmAd1*Dtheta + coefs.bmAd2*Dd + coefs.bmGd1*sin(theta) + coefs.bmGd2*d + coefs.bmBd*tau

    dX = [Dtheta;
          bmAth1*Dtheta + bmAth2*Dd + bmGth1*sin(theta) + bmGth2*d + bmBth*tau;
          Dd;
          bmAd1*Dtheta + bmAd2*Dd + bmGd1*sin(theta) + bmGd2*d + bmBd*tau];

end
