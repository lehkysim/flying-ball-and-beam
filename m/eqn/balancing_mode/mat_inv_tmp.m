syms theta(t) d(t) tau(t)
syms d1 m1 r1 b1 I1 m2 r2 b2 I2 g

% first derivatives
Dtheta(t) = diff(theta(t),t);
Dd(t) = diff(d,t);

Ms = @(d1,m1,r1,b1,I1,m2,r2,b2,I2,g) ([m2*d(t)^2+I1+4*I2,   2*I2/r2; ...
                                            2*I2/r2,      m2+I2/r2^2]);
M = vpa(Ms(prms.d1,prms.m1,prms.r1,prms.b1,prms.I1,prms.m2,prms.r2,prms.b2,prms.I2,prms.g),4);

Cs = @(d1,m1,r1,b1,I1,m2,r2,b2,I2,g) ([ 2*m2*Dd(t)*d(t)+b1+b2,   b2/r2; ...
                                       b2/r2-m2*Dtheta(t)*d(t), b2/r2^2]);
C = vpa(Cs(prms.d1,prms.m1,prms.r1,prms.b1,prms.I1,prms.m2,prms.r2,prms.b2,prms.I2,prms.g),4);

Me = subs(M,{Dtheta,theta,Dd,d}, ...
            {'Dtheta','theta','Dd','d'});

Ce = subs(C,{Dtheta,theta,Dd,d}, ...
            {'Dtheta','theta','Dd','d'});

A = -Me\Ce;

A = simplify(A);
A = expand(A);
A = collect(A);
