function [A, B] = bal_mat(X, coefs)
    
    % X = [theta; Dtheta; d; Dd]
    theta = X(1);

    bmAth1 = coefs.bmAth1;
    bmAth2 = coefs.bmAth2;
    bmGth1 = coefs.bmGth1;
    bmGth2 = coefs.bmGth2;
    bmBth  = coefs.bmBth;

    bmAd1 = coefs.bmAd1;
    bmAd2 = coefs.bmAd2;
    bmGd1 = coefs.bmGd1;
    bmGd2 = coefs.bmGd2;
    bmBd  = coefs.bmBd;

    % DDtheta = coefs.bmAth1*Dtheta + coefs.bmAth2*Dd + coefs.bmGth1*sin(theta) + coefs.bmGth2*d + coefs.bmBth*tau
    % DDd = coefs.bmAd1*Dtheta + coefs.bmAd2*Dd + coefs.bmGd1*sin(theta) + coefs.bmGd2*d + coefs.bmBd*tau

    a21 = bmGth1*cos(theta);
    a41 = bmGd1*cos(theta);

    A = [0      1       0       0;
         a21    bmAth1  bmGth2  bmAth2;   
         0      1       0       0;
         a41    bmAd1   bmGd2   bmAd2];

    B = [0; bmBth; 0; bmBd];
    
end

