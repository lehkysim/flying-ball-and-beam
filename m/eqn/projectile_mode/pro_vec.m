function dX = pro_vec(X, prms)

    % X = [x; Dx; y; Dy];
    x  = X(1);
    Dx = X(2);
    y  = X(3);
    Dy = X(4);

    % DDx = 0
    % DDy = -g
    % DDalpha = 0

    dX = [Dx; 
          0;
          Dy;
          -prms.g];

end
