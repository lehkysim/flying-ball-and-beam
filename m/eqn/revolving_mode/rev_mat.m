function [A, B] = rev_mat(X, coefs)
    
    % x = [theta; Dtheta; alpha; Dalpha];
    theta  = X(1);
    alpha  = X(3);

    rmAth1 = coefs.rmAth1;
    rmAth2 = coefs.rmAth2;
    rmGth1 = coefs.rmGth1;
    rmGth2 = coefs.rmGth2;
    rmGth3 = coefs.rmGth3;
    rmBth  = coefs.rmBth;

    rmAalp1 = coefs.rmAalp1;
    rmAalp2 = coefs.rmAalp2;
    rmGalp1 = coefs.rmGalp1;
    rmGalp2 = coefs.rmGalp2;
    rmBalp  = coefs.rmBalp;

    % DDtheta = coefs.rmAth1*Dtheta + coefs.rmAth2*Dalpha + coefs.rmGth1*cos(theta) + coefs.rmGth2*cos(theta+alpha) + coefs.rmGth3*cos(theta+2*alpha) + coefs.rmBth*tau
    % DDalpha = coefs.rmAalp1*Dtheta + coefs.rmAalp2*Dalpha + coefs.rmGalp1*cos(theta) + coefs.rmGalp2*cos(theta+alpha) + coefs.rmBalp*tau

    a21 = -rmGth1*sin(theta) - rmGth2*sin(theta+alpha) - rmGth3*sin(theta+2*alpha);
    a23 = -rmGth2*sin(theta+alpha) - 2*rmGth3*sin(theta+2*alpha);
    a41 = -rmGalp1*sin(theta) - rmGalp2*sin(theta+alpha);
    a43 = -rmGalp2*sin(theta+alpha);

    A = [0      1       0       0;
         a21    rmAth1  a23     rmAth2;
         0      0       0       1;
         a41    rmAalp1 a43     rmAalp2];

    B = [0; rmBth; 0; rmBalp];
    
end

