%% GET RM COEFFICIENTS

rmAth1 = coefs.rmAth1;
rmAth2 = coefs.rmAth2;
rmGth1 = coefs.rmGth1;
rmGth2 = coefs.rmGth2;
rmGth3 = coefs.rmGth3;
rmBth  = coefs.rmBth;

rmAalp1 = coefs.rmAalp1;
rmAalp2 = coefs.rmAalp2;
rmGalp1 = coefs.rmGalp1;
rmGalp2 = coefs.rmGalp2;
rmBalp  = coefs.rmBalp;

%% LINEARIZE

syms theta Dtheta alpha Dalpha tau

theta_dot  = Dtheta; 
alpha_dot  = Dalpha; 
Dtheta_dot = rmAth1*theta_dot + rmAth2*alpha_dot + rmGth1*cos(theta) + rmGth2*cos(theta+alpha) + rmGth3*cos(theta+2*alpha) + rmBth*tau;
Dalpha_dot = rmAalp1*theta_dot + rmAalp2*alpha_dot + rmGalp1*cos(theta) + rmGalp2*cos(theta+alpha) + rmBalp*tau;

f = [theta_dot; 
     Dtheta_dot; 
     alpha_dot; 
     Dalpha_dot];

% operating point
xp = [0; 0; 0; 0];
taup = 0;

% matrix A
A = jacobian(f, [theta, Dtheta, alpha, Dalpha]);
A = double(subs(A,{'theta','Dtheta','alpha','Dalpha'},{xp(1),xp(2),xp(3),xp(4)}));

% matrix B
B = diff(f, tau);
B = double(subs(B,{'theta','Dtheta','alpha','Dalpha','tau'},{xp(1),xp(2),xp(3),xp(4),taup}));

% matrix C
C = [1 0 0 0;
     0 0 0 0; 
     0 0 1 0; 
     0 0 0 0];

% matrix D
D = 0;

rm_sys = ss(A,B,C,D);
% discrete state-space
rm_sys_d = c2d(rm_sys,prms.Ts,'zoh');
A_d = rm_sys_d.A;
B_d = rm_sys_d.B;
C_d = rm_sys_d.C;
D_d = rm_sys_d.D;

%% LQR

Q = C_d'*C_d;
R = 1;

[K,S,P] = lqr(bm_sys_d, Q, R);
