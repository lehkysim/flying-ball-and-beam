function dX = rev_vec(X, tau, coefs)

    % x = [theta; Dtheta; alpha; Dalpha];
    theta  = X(1);
    Dtheta = X(2);
    alpha  = X(3);
    Dalpha = X(4);

    rmAth1 = coefs.rmAth1;
    rmAth2 = coefs.rmAth2;
    rmGth1 = coefs.rmGth1;
    rmGth2 = coefs.rmGth2;
    rmGth3 = coefs.rmGth3;
    rmBth  = coefs.rmBth;

    rmAalp1 = coefs.rmAalp1;
    rmAalp2 = coefs.rmAalp2;
    rmGalp1 = coefs.rmGalp1;
    rmGalp2 = coefs.rmGalp2;
    rmBalp  = coefs.rmBalp;
    
    % DDtheta = coefs.rmAth1*Dtheta + coefs.rmAth2*Dalpha + coefs.rmGth1*cos(theta) + coefs.rmGth2*cos(theta+alpha) + coefs.rmGth3*cos(theta+2*alpha) + coefs.rmBth*tau
    % DDalpha = coefs.rmAalp1*Dtheta + coefs.rmAalp2*Dalpha + coefs.rmGalp1*cos(theta) + coefs.rmGalp2*cos(theta+alpha) + coefs.rmBalp*tau

    dX = [Dtheta;
          rmAth1*Dtheta + rmAth2*Dalpha + rmGth1*cos(theta) + rmGth2*cos(theta+alpha) + rmGth3*cos(theta+2*alpha) + rmBth*tau;
          Dalpha;
          rmAalp1*Dtheta + rmAalp2*Dalpha + rmGalp1*cos(theta) + rmGalp2*cos(theta+alpha) + rmBalp*tau];

end
