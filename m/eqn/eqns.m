clear coefs;

syms theta(t) d(t) alpha(t)

%% BALANCING MODE

% EQN THETA
bmAth1s = @(d1,m1,r1,b1,I1,m2,r2,b2,I2,g) (-(I2*b1-I2*b2+b1*m2*r2^2+b2*m2*r2^2+2*m2^2*r2^2*d(t)*diff(d(t),t)+2*I2*m2*d(t)*diff(d(t),t)+2*I2*m2*r2*d(t)*diff(theta(t),t))/(I1*I2+m2^2*r2^2*d(t)^2+I2*m2*d(t)^2+I1*m2*r2^2+4*I2*m2*r2^2));
bmAth2s = @(d1,m1,r1,b1,I1,m2,r2,b2,I2,g) ((-b2*m2*r2^2+I2*b2)/(r2*(I1*I2+m2^2*r2^2*d(t)^2+I2*m2*d(t)^2+I1*m2*r2^2+4*I2*m2*r2^2)));
bmGth1s = @(d1,m1,r1,b1,I1,m2,r2,b2,I2,g) ((g*m2^2*r2^3+g*m2^2*r1*r2^2+I2*g*m2*r1+3*I2*g*m2*r2)/(I1*I2+m2^2*r2^2*d(t)^2+I2*m2*d(t)^2+I1*m2*r2^2+4*I2*m2*r2^2));
bmGth2s = @(d1,m1,r1,b1,I1,m2,r2,b2,I2,g) ((-g*m2^2*r2^2-I2*g*m2)/(I1*I2+m2^2*r2^2*d(t)^2+I2*m2*d(t)^2+I1*m2*r2^2+4*I2*m2*r2^2)*cos(theta(t)));
bmBths  = @(d1,m1,r1,b1,I1,m2,r2,b2,I2,g) ((m2*r2^2+I2)/(I1*I2+m2^2*r2^2*d(t)^2+I2*m2*d(t)^2+I1*m2*r2^2+4*I2*m2*r2^2));

bmAth1 = vpa(bmAth1s(prms.d1,prms.m1,prms.r1,prms.b1,prms.I1,prms.m2,prms.r2,prms.b2,prms.I2,prms.g),4);
bmAth2 = vpa(bmAth2s(prms.d1,prms.m1,prms.r1,prms.b1,prms.I1,prms.m2,prms.r2,prms.b2,prms.I2,prms.g),4);
bmGth1 = vpa(bmGth1s(prms.d1,prms.m1,prms.r1,prms.b1,prms.I1,prms.m2,prms.r2,prms.b2,prms.I2,prms.g),4);
bmGth2 = vpa(bmGth2s(prms.d1,prms.m1,prms.r1,prms.b1,prms.I1,prms.m2,prms.r2,prms.b2,prms.I2,prms.g),4);
bmBth  = vpa(bmBths(prms.d1,prms.m1,prms.r1,prms.b1,prms.I1,prms.m2,prms.r2,prms.b2,prms.I2,prms.g),4);

coefs.bmAth1 = -((2.994e-7*d(t)*diff(d(t),t)+8.554e-10*d(t)*diff(theta(t),t)+2.188e-9))/(1.497e-7*d(t)^2+2.451e-9);
coefs.bmAth2 = -5.05e-12/(1.497e-9*d(t)^2+2.451e-11);
coefs.bmGth1 =  4.84e-8/(1.497e-7*d(t)^2 + 2.451e-9);
coefs.bmGth2 = -(1.468e-6*cos(theta(t)))/(1.497e-7*d(t)^2+2.451e-9);
coefs.bmBth  =  4.578e-6/(1.497e-7*d(t)^2 + 2.451e-9);
% DDtheta = coefs.bmAth1*Dtheta + coefs.bmAth2*Dd + coefs.bmGth1*sin(theta) + coefs.bmGth2*d + coefs.bmBth*tau

% EQN D
bmAd1s = @(d1,m1,r1,b1,I1,m2,r2,b2,I2,g) ((2*I2*b1*r2-I1*b2*r2-2*I2*b2*r2-b2*m2*r2*d(t)^2+m2^2*r2^2*d(t)^3*diff(theta(t),t)+4*I2*m2*r2*d(t)*diff(d(t),t)+I1*m2*r2^2*d(t)*diff(theta(t),t)+4*I2*m2*r2^2*d(t)*diff(theta(t),t))/(I1*I2+m2^2*r2^2*d(t)^2+I2*m2*d(t)^2+I1*m2*r2^2+4*I2*m2*r2^2));
bmAd2s = @(d1,m1,r1,b1,I1,m2,r2,b2,I2,g) (-(b2*(I1+2*I2+m2*d(t)^2))/(I1*I2+m2^2*r2^2*d(t)^2+I2*m2*d(t)^2+I1*m2*r2^2+4*I2*m2*r2^2));
bmGd1s = @(d1,m1,r1,b1,I1,m2,r2,b2,I2,g) ((-I1*g*m2*r2^1-2*I2*g*m2*r1*r2-6*I2*g*m2*r2^2-g*m2^2*r2^2*d(t)^2)/(I1*I2+m2^2*r2^2*d(t)^2+I2*m2*d(t)^2+I1*m2*r2^2+4*I2*m2*r2^2));
bmGd2s = @(d1,m1,r1,b1,I1,m2,r2,b2,I2,g) ((2*I2*g*m2*r2*cos(theta(t)))/(I1*I2+m2^2*r2^2*d(t)^2+I2*m2*d(t)^2+I1*m2*r2^2+4*I2*m2*r2^2));
bmBds  = @(d1,m1,r1,b1,I1,m2,r2,b2,I2,g) (-(2*I2*r2)/(I1*I2+m2^2*r2^2*d(t)^2+I2*m2*d(t)^2+I1*m2*r2^2+4*I2*m2*r2^2));

bmAd1 = vpa(bmAd1s(prms.d1,prms.m1,prms.r1,prms.b1,prms.I1,prms.m2,prms.r2,prms.b2,prms.I2,prms.g),4);
bmAd2 = vpa(bmAd2s(prms.d1,prms.m1,prms.r1,prms.b1,prms.I1,prms.m2,prms.r2,prms.b2,prms.I2,prms.g),4);
bmGd1 = vpa(bmGd1s(prms.d1,prms.m1,prms.r1,prms.b1,prms.I1,prms.m2,prms.r2,prms.b2,prms.I2,prms.g),4);
bmGd2 = vpa(bmGd2s(prms.d1,prms.m1,prms.r1,prms.b1,prms.I1,prms.m2,prms.r2,prms.b2,prms.I2,prms.g),4); 
bmBd  = vpa(bmBds(prms.d1,prms.m1,prms.r1,prms.b1,prms.I1,prms.m2,prms.r2,prms.b2,prms.I2,prms.g),4);

coefs.bmAd1 =  (1.069e-7*d(t)^3*diff(theta(t),t)-8.417e-10*d(t)^2+1.711e-9*d(t)*diff(d(t),t)+1.755e-9*d(t)*diff(theta(t),t)-1.278e-12)/(1.497e-7*d(t)^2+2.451e-9);
coefs.bmAd2 = -((8.417e-8*d(t)^2+1.375e-9))/(1.497e-7*d(t)^2+2.451e-9);
coefs.bmGd1 = -((1.049e-6*d(t)^2+1.705e-6))/(1.497e-7*d(t)^2+2.451e-9);
coefs.bmGd2 =  (8.389e-9*cos(theta(t)))/(1.497e-7*d(t)^2+2.451e-9);
coefs.bmBd  = -2.616e-8/(1.497e-7*d(t)^2+2.451e-9);
% DDd = coefs.bmAd1*Dtheta + coefs.bmAd2*Dd + coefs.bmGd1*sin(theta) + coefs.bmGd2*d + coefs.bmBd*tau

%% REVOLVING MODE

% EQN THETA
rmAth1s = @(d1,m1,r1,b1,I1,m2,r2,b2,I2,g) ((4*I2*b2*r1^2-4*I2*b1*r1^2-4*b1*m2*r2^4-4*b2*m2*r2^4-8*b1*m2*r1*r2^3-4*b2*m2*r1*r2^3+4*b2*m2*r1^3*r2-4*b1*m2*r1^2*r2^2+4*b2*m2*r1^2*r2^2+(d1^2*m2^2*r2^4*sin(2*alpha(t))*diff(theta(t),t))/2+4*d1*m2^2*r2^5*sin(alpha(t))*diff(alpha(t),t)+2*d1*m2^2*r2^5*sin(alpha(t))*diff(theta(t),t)+2*b2*d1*m2*r1*r2^2*cos(alpha(t))+2*b2*d1*m2*r1^2*r2*cos(alpha(t))+12*d1*m2^2*r1^2*r2^3*sin(alpha(t))*diff(alpha(t),t)+4*d1*m2^2*r1^3*r2^2*sin(alpha(t))*diff(alpha(t),t)+6*d1*m2^2*r1^2*r2^3*sin(alpha(t))*diff(theta(t),t)+2*d1*m2^2*r1^3*r2^2*sin(alpha(t))*diff(theta(t),t)+4*I2*d1*m2*r1^3*sin(alpha(t))*diff(alpha(t),t)+d1^2*m2^2*r1*r2^3*sin(2*alpha(t))*diff(theta(t),t)+12*d1*m2^2*r1*r2^4*sin(alpha(t))*diff(alpha(t),t)+6*d1*m2^2*r1*r2^4*sin(alpha(t))*diff(theta(t),t)+(d1^2*m2^2*r1^2*r2^2*sin(2*alpha(t))*diff(theta(t),t))/2+4*I2*d1*m2*r1^2*r2*sin(alpha(t))*diff(alpha(t),t)+4*I2*d1*m2*r1*r2^2*sin(alpha(t))*diff(theta(t),t)+4*I2*d1*m2*r1^2*r2*sin(alpha(t))*diff(theta(t),t))/(d1^2*m2^2*r2^4+4*I1*I2*r1^2+4*I1*m2*r2^4+4*I2*m2*r1^4+16*I2*m2*r2^4+2*d1^2*m2^2*r1*r2^3+8*I1*m2*r1*r2^3+16*I2*m2*r1*r2^3-8*I2*m2*r1^3*r2-d1^2*m2^2*r2^4*cos(alpha(t))^2+d1^2*m2^2*r1^2*r2^2+I2*d1^2*m2*r1^2+4*I1*m2*r1^2*r2^2-12*I2*m2*r1^2*r2^2-d1^2*m2^2*r1^2*r2^2*cos(alpha(t))^2+4*I2*d1*m2*r1^3*cos(alpha(t))-2*d1^2*m2^2*r1*r2^3*cos(alpha(t))^2-8*I2*d1*m2*r1*r2^2*cos(alpha(t))-4*I2*d1*m2*r1^2*r2*cos(alpha(t))));
rmAth2s = @(d1,m1,r1,b1,I1,m2,r2,b2,I2,g) ((2*(2*I2*b2*r1^3-2*b2*m2*r1*r2^4+2*b2*m2*r1^4*r2-2*b2*m2*r1^2*r2^3+2*b2*m2*r1^3*r2^2+d1*m2^2*r2^6*sin(alpha(t))*diff(alpha(t),t)+b2*d1*m2*r1^3*r2*cos(alpha(t))+3*d1*m2^2*r1^2*r2^4*sin(alpha(t))*diff(alpha(t),t)+d1*m2^2*r1^3*r2^3*sin(alpha(t))*diff(alpha(t),t)+b2*d1*m2*r1^2*r2^2*cos(alpha(t))+3*d1*m2^2*r1*r2^5*sin(alpha(t))*diff(alpha(t),t)+I2*d1*m2*r1^2*r2^2*sin(alpha(t))*diff(alpha(t),t)+I2*d1*m2*r1^3*r2*sin(alpha(t))*diff(alpha(t),t)))/(r2*(d1^2*m2^2*r2^4+4*I1*I2*r1^2+4*I1*m2*r2^4+4*I2*m2*r1^4+16*I2*m2*r2^4+2*d1^2*m2^2*r1*r2^3+8*I1*m2*r1*r2^3+16*I2*m2*r1*r2^3-8*I2*m2*r1^3*r2-d1^2*m2^2*r2^4*cos(alpha(t))^2+d1^2*m2^2*r1^2*r2^2+I2*d1^2*m2*r1^2+4*I1*m2*r1^2*r2^2-12*I2*m2*r1^2*r2^2-d1^2*m2^2*r1^2*r2^2*cos(alpha(t))^2+4*I2*d1*m2*r1^3*cos(alpha(t))-2*d1^2*m2^2*r1*r2^3*cos(alpha(t))^2-8*I2*d1*m2*r1*r2^2*cos(alpha(t))-4*I2*d1*m2*r1^2*r2*cos(alpha(t)))));
rmGth1s = @(d1,m1,r1,b1,I1,m2,r2,b2,I2,g) ((-d1*g*m2^2*r2^4-2*d1*g*m2^2*r1*r2^3-d1*g*m2^2*r1^2*r2^2-2*I2*d1*g*m2*r1^2)/(d1^2*m2^2*r2^4+4*I1*I2*r1^2+4*I1*m2*r2^4+4*I2*m2*r1^4+16*I2*m2*r2^4+2*d1^2*m2^2*r1*r2^3+8*I1*m2*r1*r2^3+16*I2*m2*r1*r2^3-8*I2*m2*r1^3*r2-d1^2*m2^2*r2^4*cos(alpha(t))^2+d1^2*m2^2*r1^2*r2^2+I2*d1^2*m2*r1^2+4*I1*m2*r1^2*r2^2-12*I2*m2*r1^2*r2^2-d1^2*m2^2*r1^2*r2^2*cos(alpha(t))^2+4*I2*d1*m2*r1^3*cos(alpha(t))-2*d1^2*m2^2*r1*r2^3*cos(alpha(t))^2-8*I2*d1*m2*r1*r2^2*cos(alpha(t))-4*I2*d1*m2*r1^2*r2*cos(alpha(t))));
rmGth2s = @(d1,m1,r1,b1,I1,m2,r2,b2,I2,g) ((-4*I2*g*m2*r1^3+8*I2*g*m2*r1*r2^2+4*I2*g*m2*r1^2*r2)/(d1^2*m2^2*r2^4+4*I1*I2*r1^2+4*I1*m2*r2^4+4*I2*m2*r1^4+16*I2*m2*r2^4+2*d1^2*m2^2*r1*r2^3+8*I1*m2*r1*r2^3+16*I2*m2*r1*r2^3-8*I2*m2*r1^3*r2-d1^2*m2^2*r2^4*cos(alpha(t))^2+d1^2*m2^2*r1^2*r2^2+I2*d1^2*m2*r1^2+4*I1*m2*r1^2*r2^2-12*I2*m2*r1^2*r2^2-d1^2*m2^2*r1^2*r2^2*cos(alpha(t))^2+4*I2*d1*m2*r1^3*cos(alpha(t))-2*d1^2*m2^2*r1*r2^3*cos(alpha(t))^2-8*I2*d1*m2*r1*r2^2*cos(alpha(t))-4*I2*d1*m2*r1^2*r2*cos(alpha(t))));
rmGth3s = @(d1,m1,r1,b1,I1,m2,r2,b2,I2,g) ((d1*g*m2^2*r2^4+d1*g*m2^2*r1^2*r2^2+2*d1*g*m2^2*r1*r2^3)/(d1^2*m2^2*r2^4+4*I1*I2*r1^2+4*I1*m2*r2^4+4*I2*m2*r1^4+16*I2*m2*r2^4+2*d1^2*m2^2*r1*r2^3+8*I1*m2*r1*r2^3+16*I2*m2*r1*r2^3-8*I2*m2*r1^3*r2-d1^2*m2^2*r2^4*cos(alpha(t))^2+d1^2*m2^2*r1^2*r2^2+I2*d1^2*m2*r1^2+4*I1*m2*r1^2*r2^2-12*I2*m2*r1^2*r2^2-d1^2*m2^2*r1^2*r2^2*cos(alpha(t))^2+4*I2*d1*m2*r1^3*cos(alpha(t))-2*d1^2*m2^2*r1*r2^3*cos(alpha(t))^2-8*I2*d1*m2*r1*r2^2*cos(alpha(t))-4*I2*d1*m2*r1^2*r2*cos(alpha(t))));
rmBths  = @(d1,m1,r1,b1,I1,m2,r2,b2,I2,g) ((4*(m2*r1^2*r2^2+I2*r1^2+2*m2*r1*r2^3+m2*r2^4))/(d1^2*m2^2*r2^4+4*I1*I2*r1^2+4*I1*m2*r2^4+4*I2*m2*r1^4+16*I2*m2*r2^4+2*d1^2*m2^2*r1*r2^3+8*I1*m2*r1*r2^3+16*I2*m2*r1*r2^3-8*I2*m2*r1^3*r2-d1^2*m2^2*r2^4*cos(alpha(t))^2+d1^2*m2^2*r1^2*r2^2+I2*d1^2*m2*r1^2+4*I1*m2*r1^2*r2^2-12*I2*m2*r1^2*r2^2-d1^2*m2^2*r1^2*r2^2*cos(alpha(t))^2+4*I2*d1*m2*r1^3*cos(alpha(t))-2*d1^2*m2^2*r1*r2^3*cos(alpha(t))^2-8*I2*d1*m2*r1*r2^2*cos(alpha(t))-4*I2*d1*m2*r1^2*r2*cos(alpha(t))));

rmAth1 = vpa(rmAth1s(prms.d1,prms.m1,prms.r1,prms.b1,prms.I1,prms.m2,prms.r2,prms.b2,prms.I2,prms.g),4);
rmAth2 = vpa(rmAth2s(prms.d1,prms.m1,prms.r1,prms.b1,prms.I1,prms.m2,prms.r2,prms.b2,prms.I2,prms.g),4);
rmGth1 = vpa(rmGth1s(prms.d1,prms.m1,prms.r1,prms.b1,prms.I1,prms.m2,prms.r2,prms.b2,prms.I2,prms.g),4);
rmGth2 = vpa(rmGth2s(prms.d1,prms.m1,prms.r1,prms.b1,prms.I1,prms.m2,prms.r2,prms.b2,prms.I2,prms.g),4);
rmGth3 = vpa(rmGth3s(prms.d1,prms.m1,prms.r1,prms.b1,prms.I1,prms.m2,prms.r2,prms.b2,prms.I2,prms.g),4);
rmBth  = vpa(rmBths(prms.d1,prms.m1,prms.r1,prms.b1,prms.I1,prms.m2,prms.r2,prms.b2,prms.I2,prms.g),4);

coefs.rmAth1 = -((1.349e-13*cos(alpha(t))+1.154e-12*sin(2.0*alpha(t))*diff(theta(t),t)+1.712e-12*sin(alpha(t))*diff(alpha(t),t)+8.749e-13*sin(alpha(t))*diff(theta(t),t)-5.351e-12))/(3.771e-14*cos(alpha(t))+2.308e-12*cos(alpha(t))^2-8.67e-12);
coefs.rmAth2 = -((2.327e-15*cos(alpha(t))+8.561e-15*sin(alpha(t))*diff(alpha(t),t)+3.818e-16))/(3.771e-16*cos(alpha(t))+2.308e-14*cos(alpha(t))^2-8.67e-14);
coefs.rmGth1 =  1.753e-10/(3.771e-14*cos(alpha(t))+2.308e-12*cos(alpha(t))^2-8.67e-12);
coefs.rmGth2 = -2.169e-12/(3.771e-14*cos(alpha(t))+2.308e-12*cos(alpha(t))^2-8.67e-12);
coefs.rmGth3 = -1.328e-10/(3.771e-14*cos(alpha(t))+2.308e-12*cos(alpha(t))^2-8.67e-12);
coefs.rmBth  = -1.127e-8/(3.771e-14*cos(alpha(t))+2.308e-12*cos(alpha(t))^2-8.67e-12);
% DDtheta = coefs.rmAth1*Dtheta + coefs.rmAth2*Dalpha + coefs.rmGth1*cos(theta) + coefs.rmGth2*cos(alpha+theta) + coefs.rmGth3*cos(theta+2*alpha) + coefs.rmBth*tau

% EQN ALPHA
rmAalp1s = @(d1,m1,r1,b1,I1,m2,r2,b2,I2,g) (-(8*b2*m2*r1^3*r2-8*b2*m2*r2^4-16*b1*m2*r1*r2^3-8*b2*m2*r1*r2^3-8*b1*m2*r2^4-8*b1*m2*r1^2*r2^2+8*b2*m2*r1^2*r2^2+8*I1*b2*r1*r2-16*I2*b1*r1*r2+16*I2*b2*r1*r2+2*d1^2*m2^2*r2^4*sin(2*alpha(t))*diff(alpha(t),t)+2*d1^2*m2^2*r2^4*sin(2*alpha(t))*diff(theta(t),t)+8*d1*m2^2*r2^5*sin(alpha(t))*diff(alpha(t),t)+4*d1*m2^2*r2^5*sin(alpha(t))*diff(theta(t),t)-4*b1*d1*m2*r2^3*cos(alpha(t))-4*b2*d1*m2*r2^3*cos(alpha(t))+d1^3*m2^2*r2^3*sin(alpha(t))*diff(theta(t),t)+2*b2*d1^2*m2*r1*r2-4*b1*d1*m2*r1*r2^2*cos(alpha(t))+4*b2*d1*m2*r1*r2^2*cos(alpha(t))+8*b2*d1*m2*r1^2*r2*cos(alpha(t))+24*d1*m2^2*r1^2*r2^3*sin(alpha(t))*diff(alpha(t),t)+8*d1*m2^2*r1^3*r2^2*sin(alpha(t))*diff(alpha(t),t)+12*d1*m2^2*r1^2*r2^3*sin(alpha(t))*diff(theta(t),t)+4*d1*m2^2*r1^3*r2^2*sin(alpha(t))*diff(theta(t),t)+d1^3*m2^2*r1*r2^2*sin(alpha(t))*diff(theta(t),t)+4*I1*d1*m2*r2^3*sin(alpha(t))*diff(theta(t),t)+16*I2*d1*m2*r2^3*sin(alpha(t))*diff(theta(t),t)+4*d1^2*m2^2*r1*r2^3*sin(2*alpha(t))*diff(alpha(t),t)+4*d1^2*m2^2*r1*r2^3*sin(2*alpha(t))*diff(theta(t),t)+24*d1*m2^2*r1*r2^4*sin(alpha(t))*diff(alpha(t),t)+12*d1*m2^2*r1*r2^4*sin(alpha(t))*diff(theta(t),t)+2*d1^2*m2^2*r1^2*r2^2*sin(2*alpha(t))*diff(alpha(t),t)+2*d1^2*m2^2*r1^2*r2^2*sin(2*alpha(t))*diff(theta(t),t)+16*I2*d1*m2*r1*r2^2*sin(alpha(t))*diff(alpha(t),t)+16*I2*d1*m2*r1^2*r2*sin(alpha(t))*diff(alpha(t),t)+4*I1*d1*m2*r1*r2^2*sin(alpha(t))*diff(theta(t),t)+16*I2*d1*m2*r1*r2^2*sin(alpha(t))*diff(theta(t),t))/(2*(d1^2*m2^2*r2^4+4*I1*I2*r1^2+4*I1*m2*r2^4+4*I2*m2*r1^4+16*I2*m2*r2^4+2*d1^2*m2^2*r1*r2^3+8*I1*m2*r1*r2^3+16*I2*m2*r1*r2^3-8*I2*m2*r1^3*r2-d1^2*m2^2*r2^4*cos(alpha(t))^2+d1^2*m2^2*r1^2*r2^2+I2*d1^2*m2*r1^2+4*I1*m2*r1^2*r2^2-12*I2*m2*r1^2*r2^2-d1^2*m2^2*r1^2*r2^2*cos(alpha(t))^2+4*I2*d1*m2*r1^3*cos(alpha(t))-2*d1^2*m2^2*r1*r2^3*cos(alpha(t))^2-8*I2*d1*m2*r1*r2^2*cos(alpha(t))-4*I2*d1*m2*r1^2*r2*cos(alpha(t)))));
rmAalp2s = @(d1,m1,r1,b1,I1,m2,r2,b2,I2,g) (-(4*I1*b2*r1^2+8*I2*b2*r1^2+4*b2*m2*r1^4-4*b2*m2*r1*r2^3+4*b2*m2*r1^3*r2+b2*d1^2*m2*r1^2-4*b2*m2*r1^2*r2^2+(d1^2*m2^2*r2^4*sin(2*alpha(t))*diff(alpha(t),t))/2+2*d1*m2^2*r2^5*sin(alpha(t))*diff(alpha(t),t)+4*b2*d1*m2*r1^3*cos(alpha(t))-2*b2*d1*m2*r1*r2^2*cos(alpha(t))+2*b2*d1*m2*r1^2*r2*cos(alpha(t))+6*d1*m2^2*r1^2*r2^3*sin(alpha(t))*diff(alpha(t),t)+2*d1*m2^2*r1^3*r2^2*sin(alpha(t))*diff(alpha(t),t)+d1^2*m2^2*r1*r2^3*sin(2*alpha(t))*diff(alpha(t),t)+6*d1*m2^2*r1*r2^4*sin(alpha(t))*diff(alpha(t),t)+(d1^2*m2^2*r1^2*r2^2*sin(2*alpha(t))*diff(alpha(t),t))/2+4*I2*d1*m2*r1*r2^2*sin(alpha(t))*diff(alpha(t),t)+4*I2*d1*m2*r1^2*r2*sin(alpha(t))*diff(alpha(t),t))/(d1^2*m2^2*r2^4+4*I1*I2*r1^2+4*I1*m2*r2^4+4*I2*m2*r1^4+16*I2*m2*r2^4+2*d1^2*m2^2*r1*r2^3+8*I1*m2*r1*r2^3+16*I2*m2*r1*r2^3-8*I2*m2*r1^3*r2-d1^2*m2^2*r2^4*cos(alpha(t))^2+d1^2*m2^2*r1^2*r2^2+I2*d1^2*m2*r1^2+4*I1*m2*r1^2*r2^2-12*I2*m2*r1^2*r2^2-d1^2*m2^2*r1^2*r2^2*cos(alpha(t))^2+4*I2*d1*m2*r1^3*cos(alpha(t))-2*d1^2*m2^2*r1*r2^3*cos(alpha(t))^2-8*I2*d1*m2*r1*r2^2*cos(alpha(t))-4*I2*d1*m2*r1^2*r2*cos(alpha(t))));
rmGalp1s = @(d1,m1,r1,b1,I1,m2,r2,b2,I2,g) ((2*d1*g*m2^2*r2^4+4*d1*g*m2^2*r1*r2^3+2*d1*g*m2^2*r1^2*r2^2+4*I2*d1*g*m2*r1*r2+d1^2*g*m2^2*r2^3*cos(alpha(t))+d1^2*g*m2^2*r1*r2^2*cos(alpha(t)))/(d1^2*m2^2*r2^4+4*I1*I2*r1^2+4*I1*m2*r2^4+4*I2*m2*r1^4+16*I2*m2*r2^4+2*d1^2*m2^2*r1*r2^3+8*I1*m2*r1*r2^3+16*I2*m2*r1*r2^3-8*I2*m2*r1^3*r2-d1^2*m2^2*r2^4*cos(alpha(t))^2+d1^2*m2^2*r1^2*r2^2+I2*d1^2*m2*r1^2+4*I1*m2*r1^2*r2^2-12*I2*m2*r1^2*r2^2-d1^2*m2^2*r1^2*r2^2*cos(alpha(t))^2+4*I2*d1*m2*r1^3*cos(alpha(t))-2*d1^2*m2^2*r1*r2^3*cos(alpha(t))^2-8*I2*d1*m2*r1*r2^2*cos(alpha(t))-4*I2*d1*m2*r1^2*r2*cos(alpha(t))));
rmGalp2s = @(d1,m1,r1,b1,I1,m2,r2,b2,I2,g) ((-d1^2*g*m2^2*r2^3-4*I1*g*m2*r2^3+8*I2*g*m2*r1^2*r2-16*I2*g*m2*r2^3-d1^2*g*m2^2*r1*r2^2-4*I1*g*m2*r1*r2^2-8*I2*g*m2*r1*r2^2-2*d1*g*m2^2*r2^4*cos(alpha(t))-4*d1*g*m2^2*r1*r2^3*cos(alpha(t))-2*d1*g*m2^2*r1^2*r2^2*cos(alpha(t)))/(d1^2*m2^2*r2^4+4*I1*I2*r1^2+4*I1*m2*r2^4+4*I2*m2*r1^4+16*I2*m2*r2^4+2*d1^2*m2^2*r1*r2^3+8*I1*m2*r1*r2^3+16*I2*m2*r1*r2^3-8*I2*m2*r1^3*r2-d1^2*m2^2*r2^4*cos(alpha(t))^2+d1^2*m2^2*r1^2*r2^2+I2*d1^2*m2*r1^2+4*I1*m2*r1^2*r2^2-12*I2*m2*r1^2*r2^2-d1^2*m2^2*r1^2*r2^2*cos(alpha(t))^2+4*I2*d1*m2*r1^3*cos(alpha(t))-2*d1^2*m2^2*r1*r2^3*cos(alpha(t))^2-8*I2*d1*m2*r1*r2^2*cos(alpha(t))-4*I2*d1*m2*r1^2*r2*cos(alpha(t))));
rmBalps  = @(d1,m1,r1,b1,I1,m2,r2,b2,I2,g) (-(2*r2*(2*m2*r1^2*r2+4*m2*r1*r2^2+d1*m2*cos(alpha(t))*r1*r2+4*I2*r1+2*m2*r2^3+d1*m2*cos(alpha(t))*r2^2))/(d1^2*m2^2*r2^4+4*I1*I2*r1^2+4*I1*m2*r2^4+4*I2*m2*r1^4+16*I2*m2*r2^4+2*d1^2*m2^2*r1*r2^3+8*I1*m2*r1*r2^3+16*I2*m2*r1*r2^3-8*I2*m2*r1^3*r2-d1^2*m2^2*r2^4*cos(alpha(t))^2+d1^2*m2^2*r1^2*r2^2+I2*d1^2*m2*r1^2+4*I1*m2*r1^2*r2^2-12*I2*m2*r1^2*r2^2-d1^2*m2^2*r1^2*r2^2*cos(alpha(t))^2+4*I2*d1*m2*r1^3*cos(alpha(t))-2*d1^2*m2^2*r1*r2^3*cos(alpha(t))^2-8*I2*d1*m2*r1*r2^2*cos(alpha(t))-4*I2*d1*m2*r1^2*r2*cos(alpha(t))));

rmAalp1 = vpa(rmAalp1s(prms.d1,prms.m1,prms.r1,prms.b1,prms.I1,prms.m2,prms.r2,prms.b2,prms.I2,prms.g),4);
rmAalp2 = vpa(rmAalp2s(prms.d1,prms.m1,prms.r1,prms.b1,prms.I1,prms.m2,prms.r2,prms.b2,prms.I2,prms.g),4);
rmGalp1 = vpa(rmGalp1s(prms.d1,prms.m1,prms.r1,prms.b1,prms.I1,prms.m2,prms.r2,prms.b2,prms.I2,prms.g),4);
rmGalp2 = vpa(rmGalp2s(prms.d1,prms.m1,prms.r1,prms.b1,prms.I1,prms.m2,prms.r2,prms.b2,prms.I2,prms.g),4);
rmBalp  = vpa(rmBalps(prms.d1,prms.m1,prms.r1,prms.b1,prms.I1,prms.m2,prms.r2,prms.b2,prms.I2,prms.g),4);

coefs.rmAalp1 =  (4.616e-12*sin(2*alpha(t))*diff(theta(t),t)-2.859e-11*cos(alpha(t))+3.5e-12*sin(alpha(t))*diff(alpha(t),t)+4.854e-11*sin(alpha(t))*diff(theta(t),t)+4.616e-12*sin(2.0*alpha(t))*diff(alpha(t),t)-8.205e-12)/(7.542e-14*cos(alpha(t))+4.616e-12*cos(alpha(t))^2-1.734e-11);
coefs.rmAalp2 =  (3.305e-13*cos(alpha(t))+8.749e-13*sin(alpha(t))*diff(alpha(t),t)+1.154e-12*sin(2.0*alpha(t))*diff(alpha(t),t)+2.396e-12)/(3.771e-14*cos(alpha(t))+2.308e-12*cos(alpha(t))^2-8.67e-12);
coefs.rmGalp1 = -((8.307e-10*cos(alpha(t))+3.149e-10))/(3.771e-14*cos(alpha(t))+2.308e-12*cos(alpha(t))^2-8.67e-12);
coefs.rmGalp2 =  (2.655e-10*cos(alpha(t))+2.691e-9)/(3.771e-14*cos(alpha(t))+2.308e-12*cos(alpha(t))^2-8.67e-12);
coefs.rmBalp  =  (3.039e-8*cos(alpha(t))+1.152e-8)/(3.771e-14*cos(alpha(t))+2.308e-12*cos(alpha(t))^2-8.67e-12);
% DDalpha = coefs.rmAalp1*Dtheta + coefs.rmAalp2*Dalpha + coefs.rmGalp1*cos(theta) + coefs.rmGalp2*cos(theta+alpha) + coefs.rmBalp*tau

%% SAVE TO .MAT FILE

save("coefs.mat", "coefs");
