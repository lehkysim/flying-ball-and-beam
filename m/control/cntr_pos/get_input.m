A = 8;

pos = [0 25 45 25 10 -15 -30 -15];
dur = A*ones(1,numel(pos));

Nt = floor(sum(dur./prms.Ts));
time = prms.Ts*(0:Nt-1)';

wav = zeros(Nt,1);

count = 0;

for i = 1:numel(pos)

    N = floor(dur(i)/prms.Ts);
    for j = 1:N
        wav(count+j) = pos(i); 
    end
    count = count + N;

end

plot(time, wav);
xlim([0 sum(dur)]);
ylim([min(pos)-5, max(pos)+5]);

simin.time = time;
simin.signals.values = wav;