%% FINDING TRAJECTORY FOR BALANCING BALL ON BEAM

% parameters
N = 100;
Tf = 10;
x0 = [0;0;0;0];

method = 'euler';
% method = 'rk4';

umin = -10;
umax = 10;

% initialize the optimization problem
opti = casadi.Opti();

% define decision variables
X = opti.variable(4,N+1); % states
th = X(1, :);
Dth = X(2, :);
d = X(3, :);
Dd = X(4, :);
U = opti.variable(1,N); % controls

% bbjective function (minimize the control effort)
opti.minimize(U*U');

% dynamic constraints
f = @(x,u) bab_ode(x, u);   % dx/dt = f(x,u)
Ts = Tf/N;                  % control interval length

switch lower(method)
    case 'euler'
        % ---- Forward Euler --------
        for k=1:N % loop over control intervals
            x_next = X(:,k) + Ts*f(X(:,k), U(:,k));
            opti.subject_to(X(:,k+1)==x_next); % close the gaps
        end
    case 'rk4'
        % ---- RK4 --------
        for k=1:N % loop over control intervals
            % Runge-Kutta 4 integration
            k1 = f(X(:,k),         U(:,k));
            k2 = f(X(:,k)+Ts/2*k1, U(:,k));
            k3 = f(X(:,k)+Ts/2*k2, U(:,k));
            k4 = f(X(:,k)+Ts*k3,   U(:,k));
            x_next = X(:,k) + Ts/6*(k1+2*k2+2*k3+k4);
            opti.subject_to(X(:,k+1)==x_next); % close the gaps
        end
    otherwise
        error('Unknown method was chosen!');
end

% initial conditions
opti.subject_to(th(1)==0);   % start at position 0 ...
opti.subject_to(Dth(1)==0);  % ... from stand-still 
opti.subject_to(d(1)==0.01);
opti.subject_to(Dd(1)==0);

% final-time constraints
opti.subject_to(th(end)==0);
opti.subject_to(Dth(end)==0);
opti.subject_to(d(end)==0);
opti.subject_to(Dd(end)==0);

opti.subject_to(umin<=U<=umax);
opti.subject_to(-pi/4<=th<=pi/4);

% initialize decision variables
opti.set_initial(U, 0);
opti.set_initial(th, 0);
opti.set_initial(Dth, 0);
opti.set_initial(d, linspace(0.01,0, N+1));
opti.set_initial(Dd, 0);

% solve NLP
opti.solver('ipopt'); % use IPOPT solver
sol = opti.solve();

% extract the states and control from the decision variables
x_nlp = sol.value(X)';
u_nlp = sol.value(U)';
t_nlp = (0:N)*Ts;

%% COMPARING OBTAINED TRAJ W TRAJ OBTAINED BY SOLVING BVP

figure(1)
subplot(311)
plot(t_nlp, x_nlp(:,1:2));
grid on
xlabel('Time [s]')
ylabel('States th [-]')

subplot(312)
plot(t_nlp, x_nlp(:,3:4));
grid on
xlabel('Time [s]')
ylabel('States d [-]')

subplot(313)
plot(t_nlp(1:end-1), u_nlp);
% hold on
% plot(t_star, u_star, 'r--');
% hold off
grid on
xlabel('Time [s]')
ylabel('Controls [Nm]')
legend('NLP', 'TPBVP')

%% APPLY CONTROLS FROM COMPUTEF TO CONTINOUS MODEL

u_TS.time = t_nlp(1:end-1);
u_TS.signals.values = u_nlp;

sim('pendulumFF', [t_star(1) t_star(end)]);

figure(2)
clf
stairs(t_nlp, x_nlp)
hold on
stairs(simout.Time, simout.Data, '--')
hold off
h = legend('$\theta[k]$ - NLP', '$\dot{\theta}[k]$ - NLP', ...
           '$\theta[k]$ - simulated', '$\dot{\theta}[k]$ - simulated', ...
           'Location', 'eastoutside');
set(h,'Interpreter','latex');
grid on
xlabel('Time [s]')
xlim([t_star(1) t_star(end)])

%% RUN VISUALIZATION

% visu(simout.Time, simout.Data(:,1), l)
