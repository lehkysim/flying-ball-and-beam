clear simin;

Ts = 0.005;
ks = [0.23 0 -0.3 0];
dur = [0.65 0.2 1 2];

t = @(start,dur) start:Ts:(start+dur);
lin_fcn = @(k,t,q) k*t+q;

wav_t = zeros(1,sum(dur)/Ts);
wav = zeros(1,sum(dur)/Ts);
t_last = 0;
fcn_last = 0;
N = 1;

t_all = 0;

for i = 1:numel(dur)

    time = t(t_last,dur(i));
    func = lin_fcn(ks(i),time-t_last,fcn_last);

    for j = 1:(numel(time))
        wav_t(N+j) = time(j);
        wav(N+j) = func(j);
    end

    N = N + numel(time);
    t_last = time(end);
    fcn_last = func(end);

end

plot(wav_t,wav);
xlim([0 wav_t(end)]);

simin.time = wav_t';
simin.signals.values = wav';
