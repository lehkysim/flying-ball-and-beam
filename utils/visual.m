function visual(prms)
    
%% COLORS

colors.black = [0 0 0]/255;
colors.grey_dark  = [157,153,173]/255;
colors.grey_light = [210,210,210]/255;
colors.blue_dark  = [  0,125,199]/255;
colors.blue_light = [160,220,255]/255;

c = 0.003;

%% PREPARE VARS

N = 24;

% CREATE TRAJECTORY
% prms = trajectory.get_prms;
% dt = 1/25;
% [t, x, ~] = traj.interp(dt);
% thp = x(:,1);
% phip = x(:,5);
% [xp, yp] = traj.get_xy(dt);

% TESTING DATA
t = linspace(0,5,40);
thp = linspace(0,pi/6,40);
phip = linspace(0,pi/6,40);
xp = linspace(0,-0.1,40);
yp = zeros(1,40);

%% SET FIGURE

f = figure(); 
f.Position = [100 100 640 640];
f.Color = 'w';

ax = axes(); 
grid on;
box on;

axis(1.2*prms.l1/2*[-1 1 -1 1]);
axis equal

xticks([-prms.l1/2 0 prms.l1/2]); 
yticks([-prms.l1/2 0 prms.l1/2]);

xlim([-1.2*prms.l1/2 1.2*prms.l1/2]);
ylim([-1.2*prms.l1/2 1.2*prms.l1/2]);
xlabel('World plane X');
ylabel('World plane Y');  

set(gca,'FontSize',15);

%% DRAW OBJECTS

hold on
% show beam
beam_parts = zeros(N+1,8);
beam_parts(:,1:2) = get_circ_quad(1,3,N);      % left semi-circle
beam_parts(:,3:4) = get_circ_quad(-1,1,N);     % right semi-circle
beam_parts(:,5:6) = get_circ_quad(0,4,N);      % beam axis
beam_parts(:,7:8) = [linspace(-prms.d1/2,prms.d1/2,N+1); ...  % linkage
                     zeros(1,N+1)]';

b_beam(1) = fill(ax, (prms.r1*beam_parts(:,1) - prms.d1/2*ones(N+1,1)), ...
                      prms.r1*beam_parts(:,2), colors.grey_light);
b_beam(2) = fill(ax, (prms.r1*beam_parts(:,3) + prms.d1/2*ones(N+1,1)), ...
                      prms.r1*beam_parts(:,4), colors.grey_light);
b_beam(3) = fill(ax, c*beam_parts(:,5), ...
                     c*beam_parts(:,6), colors.black);
b_beam(4) = fill(ax, beam_parts(:,7),  ...
                     prms.r1*ones(N+1,1) + beam_parts(:,8), colors.black);
b_beam(5) = fill(ax, beam_parts(:,7), ...
                    -prms.r1*ones(N+1,1) + beam_parts(:,8), colors.black);

% show ball
ball_parts = zeros(N+1,8);
ball_parts(:,1:2) = get_circ_quad(0,1,N);
ball_parts(:,3:4) = get_circ_quad(1,2,N); 
ball_parts(:,5:6) = get_circ_quad(2,3,N);
ball_parts(:,7:8) = get_circ_quad(3,4,N);

b_ball(1) = fill(ax, prms.r2*ball_parts(:,1), ...
                     prms.r2*ball_parts(:,2)+prms.r2+prms.r1, colors.blue_dark);
b_ball(2) = fill(ax, prms.r2*ball_parts(:,3), ...
                     prms.r2*ball_parts(:,4)+prms.r2+prms.r1, colors.blue_light);
b_ball(3) = fill(ax, prms.r2*ball_parts(:,5), ...
                     prms.r2*ball_parts(:,6)+prms.r2+prms.r1, colors.blue_dark);
b_ball(4) = fill(ax, prms.r2*ball_parts(:,7), ...
                     prms.r2*ball_parts(:,8)+prms.r2+prms.r1, colors.blue_light);

hold off

% display plane
% update(1,b_beam,beam_parts,thp,b_ball,ball_parts,xp,yp,phip,prms,c,N);
   
%% MAIN PROGRAM
    
    for j = 1:(numel(t)-1)
        tic
          
        % draw new ball and beam position
        update(j,b_beam,beam_parts,thp,b_ball,ball_parts,xp,yp,phip,prms,c,N);
                   
        t_elapsed = toc;
        dt = t(j+1) - t(j);
        if (dt - t_elapsed) > 0
            pause(dt - t_elapsed);
        end
    end

function update(i,b_beam,beam_parts,thp,b_ball,ball_parts,xp,yp,phip,prms,c,N)  
    draw_beam(b_beam, beam_parts, thp(i), prms, c, N);    
    draw_ball(b_ball, ball_parts, xp(i), yp(i), phip(i), prms);
    drawnow;
end
end
    
%% FUNCTIONS

function polar = get_circ_quad(k1,k2,N)
    beta = linspace(k1*pi/2,k2*pi/2,N);
    polar = [[cos(beta) 0]', [sin(beta) 0]'];
end

function [x, y] = transfer(x, y, angle, l)
    T = [cos(angle) -sin(angle) l(1); 
         sin(angle)  cos(angle) l(2); 
             0           0       1  ];
    tmp = T*[x(:)'; y(:)'; ones(1, numel(x))];
    x = tmp(1,:)';
    y = tmp(2,:)';
end

function draw_beam(b_beam, beam_parts, thp, prms, c, N)
    [x, y] = transfer(prms.r1*beam_parts(:,1)-prms.d1/2, ...
                      prms.r1*beam_parts(:,2), thp, [0,0]);
    set(b_beam(1), 'XData', x, 'YData', y);
    [x, y] = transfer(prms.r1*beam_parts(:,3)+prms.d1/2, ...
                      prms.r1*beam_parts(:,4), thp, [0,0]);
    set(b_beam(2), 'XData', x, 'YData', y);
    [x, y] = transfer(c*beam_parts(:,5), c*beam_parts(:,6), 0, [0,0]);
    set(b_beam(3), 'XData', x, 'YData', y);
    [x, y] = transfer(beam_parts(:,7), prms.r1*ones(1,N+1), thp, [0,0]);
    set(b_beam(4), 'XData', x, 'YData', y);
    [x, y] = transfer(beam_parts(:,7), -prms.r1*ones(1,N+1), thp, [0,0]);
    set(b_beam(5), 'XData', x, 'YData', y);
end

function draw_ball(b_ball, ball_parts, xp, yp, phip, prms)
    for i = 2:2:8
        [x, y] = transfer(xp+prms.r2*ball_parts(:,i-1), ...
                          yp+prms.r2*ball_parts(:,i)+prms.r2+prms.r1, ...
                          phip, [0,0]);
        set(b_ball(i/2), 'XData', x, 'YData', y);
    end
end
