classdef Trajectory
    properties (Access = private)
        t_raw;
        u_raw;
        x_raw;
        prms;
    end

    methods
        function obj = Trajectory(t, x, u, prms)
            t = change2column(t);
            % x = [theta Dtheta d Dd]
            x = change2column(x);
            u = change2column(u);
           
            if size(x,2) <= 4
                % add other parameters to vector x
                phi = x(:,1) + x(:,3)/prms.r2;
                Dphi = (x(:,2) + x(:,4))/prms.r2;

                x = [x phi Dphi];
            end
            
            if size(t,1) ~= size(x,1) || size(t,1) ~= size(u,1)
                error('The lengths of time, state and input vector differ!')
            end            
            
            obj.t_raw = t;
            obj.x_raw = x;
            obj.u_raw = u;
            obj.prms = prms;
        end
        
        function [t, x, u] = interp(obj, Ts)
            num = size(obj.x_raw,2);
            t = (0:Ts:obj.t_raw(end))';
            u = interp1(obj.t_raw, obj.u_raw, t);
            x = zeros(numel(t), num);
            for i = 1:num
                x(:,i) = interp1(obj.t_raw, obj.x_raw(:,i), t);
            end
        end
        
        function prms = get_prms(obj)
            prms = obj.prms;
        end
        
        function [x, y] = get_xy(obj, Ts)
            if nargin < 2
                x_state = obj.x_raw;
            else
                [~, x_state, ~] = obj.interp(Ts);
            end
            
            theta = x_state(:,1);
            d = x_state(:,3);
            x = d.*cos(theta) - (obj.prms.r1+obj.prms.r2).*sin(theta);
            y = d.*sin(theta) + (obj.prms.r1+obj.prms.r2).*cos(theta);
        end

        function col = change2column(vec)
            if size(vec,2) > size(vec,1)
                col = vec';
            else
                col = vec;
            end
        end

    end
end