% Minimum distance from a point (ball coords) to the line segment (beam)
function d = PLS_dist(theta, E)

    quad = get_quad(theta);

    if (quad == 1 || quad == 4)
        Ax0 = -prms.d1/2;
        Ay0 =  prms.r1;
        Bx0 =  prms.d1/2;
        By0 =  prms.r1;
    else % (quad == 2 || quad == 3)
        Ax0 =  prms.d1/2;
        Ay0 = -prms.r1;
        Bx0 = -prms.d1/2;
        By0 = -prms.r1;
    end

    R = [cos(theta) -sin(theta) 0;
         sin(theta)  cos(theta) 0;
              0           0     1];

    A = R(theta)*[Ax0; Ay0; 1];
    Ax = A(1);
    Ay = A(2);
    B = R(theta)*[Bx0; By0; 1];
    Bx = B(1);
    By = B(2);

    Ex = E(1);
    Ey = E(2);

    % SOURCE: https://www.geeksforgeeks.org/minimum-distance-from-...
    % a-point-to-the-line-segment-using-vectors/

    % vector AB
    ABx = Bx - Ax;
    ABy = By - Ay;
    % vector BE
    BEx = Ex - Bx;
    BEy = Ey - By;
    % vector AE
    AEx = Ex - Ax;
    AEy = Ey - Ay;

    % dot product
    AB_BE = ABx*BEx + ABy*BEy;
    AB_AE = ABx*AEx + ABy*AEy;

    % get distance
    if (AB_BE > 0)
        d = norm(E-B);
    elseif (AB_AE < 0)
        d = norm(E-A); 
    else    % AB_AE = 0 || AB_BE = 0
        d = abs(ABx*AEy - AEx*ABy)/norm([ABx ABy]);
    end

    function quad = get_quad(theta)

        theta = mod(theta,2*pi);
        theta = rad2deg(theta);

        if theta > 0 && theta < 89
            quad = 1;
        elseif theta > 90 && theta < 179
            quad = 2;
        elseif theta > 180 && theta < 269
            quad = 1;
        elseif theta > 270 && theta < 359
            quad = 4;
        else 
            quad = 0;
        end
    end

end
