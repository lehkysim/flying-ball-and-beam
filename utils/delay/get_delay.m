load("delay.mat");

time1 = 0;
time2 = 0;

for i = 1:(numel(time)-1)
    if abs(input(i+1) - input(i)) > 0.001
        time1 = time(i);
        break;
    end
end

for i = 1:(numel(time)-1)
    if abs(angle(i+1) - angle(i)) > 0.002
        time2 = time(i);
        break;
    end
end

delay = abs(time2 - time1);
fprintf("Delay of the system is %d ms.\n", 1000*delay);
