import cv2

g = 9.807
fps = 960
video_name = "falling_ball.mp4"
cap = cv2.VideoCapture(video_name)
frames = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))

t = frames / fps
print(t)
v = 1/2*g*pow(t,2)
print(v)
