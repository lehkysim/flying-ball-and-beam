run("params.m");
clear ball beam;

N = 24;
theta = 0;

%% SET BALL POSITION

x = 0;
y = 0;
phi = 0;
pos = [x,y,phi];

%% COLORS

colors.black = [0 0 0]/255;
colors.grey_dark  = [157,153,173]/255;
colors.grey_light = [194,188,213]/255;
colors.red_dark   = [139,0,0]/255;
colors.red_light  = [230,0,0]/255;

%% SET FIGURE

f = figure(25);
set(gcf,'Position',[100 100 640 640]);
set(gcf,'color','w');

set(gca,'YDir','Reverse');

axis(1.2*prms.r1*[-1 1 -1 1])
axis equal
xticks([]) 
yticks([])
box on
grid on

ax = axes();
xlim([-0.15 0.151]);
ylim([-0.15 0.151]);

%% PLOT

hold on
draw_beam(ax,prms,theta,N,colors)
draw_ball(ax,prms,pos,theta,N,colors)
hold off

%% FUNCS

function draw_beam(ax,prms,theta,N,colors)
    
    beam.lsmc = get_circ_quad(1,3,N,0);     % left semi-circle
    beam.rsmc = get_circ_quad(-1,1,N,0);    % right semi-circle
    beam.ctr = get_circ_quad(0,4,N,0);      % beam center

    [xp, yp] = transfer(prms.r1*beam.lsmc(:,1)-prms.d1/2, prms.r1*beam.lsmc(:,2), theta, [0,0]);
    fill(ax, xp, yp, colors.grey_dark);
    [xp, yp] = transfer(prms.r1*beam.rsmc(:,1)+prms.d1/2, prms.r1*beam.rsmc(:,2), theta, [0,0]);
    fill(ax, xp, yp, colors.grey_dark);
    [xp, yp] = transfer(prms.c1*beam.ctr(:,1), prms.c1*beam.ctr(:,2), 0, [0,0]);
    fill(ax, xp, yp, colors.black);
    linkage = linspace(-prms.d1/2,prms.d1/2,N);
    [xp, yp] = transfer(linkage, prms.r1*ones(1,N), theta, [0,0]);
    fill(ax, xp, yp, colors.black);
    [xp, yp] = transfer(linkage, -prms.r1*ones(1,N), theta, [0,0]);
    fill(ax, xp, yp, colors.black);
end

function draw_ball(ax,prms,pos,theta,N,colors)
    [x,y,phi] = feval(@(x) x{:}, num2cell(pos));
    % ball quadrants
    ball.quad1 = get_circ_quad(0,1,N,phi);
    ball.quad2 = get_circ_quad(1,2,N,phi);  
    ball.quad3 = get_circ_quad(2,3,N,phi);   
    ball.quad4 = get_circ_quad(3,4,N,phi);

    [xp, yp] = transfer(x+prms.r2*ball.quad1(:,1), y+prms.r2*ball.quad1(:,2)+prms.r2+prms.r1, theta, [0,0]);
    fill(ax, xp, yp, colors.red_dark);
    [xp, yp] = transfer(x+prms.r2*ball.quad2(:,1), y+prms.r2*ball.quad2(:,2)+prms.r2+prms.r1, theta, [0,0]);
    fill(ax, xp, yp, colors.red_light);
    [xp, yp] = transfer(x+prms.r2*ball.quad3(:,1), y+prms.r2*ball.quad3(:,2)+prms.r2+prms.r1, theta, [0,0]);
    fill(ax, xp, yp, colors.red_dark);
    [xp, yp] = transfer(x+prms.r2*ball.quad4(:,1), y+prms.r2*ball.quad4(:,2)+prms.r2+prms.r1, theta, [0,0]);
    fill(ax, xp, yp, colors.red_light);
end

function [x, y] = transfer(x, y, angle, l)
    T = [cos(angle) -sin(angle) l(1); 
         sin(angle)  cos(angle) l(2); 
             0           0       1  ];
    tmp = T*[x(:)'; y(:)'; ones(1, numel(x))];
    x = tmp(1,:)';
    y = tmp(2,:)';
end

function polar = get_circ_quad(k1,k2,N,phi)
    beta = linspace(k1*pi/2,k2*pi/2,N);
    polar = [[cos(beta+phi) 0]', [sin(beta+phi) 0]'];
end
