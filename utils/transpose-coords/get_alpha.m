function alpha = get_alpha(theta, xy)
    
    x = xy(1);
    y = xy(2);
    
    R = [cos(theta) -sin(theta);
         sin(theta)  cos(theta)];

    px = 0;
    % py = prms.d1/2;
    py = 85.3; % [mm]
    
    P = R*[px; py];
    Px = P(1);
    Py = P(2);

    alpha = -atan2(x-Px,y-Py);

end