function d = get_d(theta, xy)

    x = xy(1);
    y = xy(2);
    
    R = [cos(theta) -sin(theta);
         sin(theta)  cos(theta)];
    
    px = 0;
    % oy = prms.r1 + prms.r2 - prms.g1;
    py = 24.3;  % [mm]
    
    P = R*[px; py];
    Px = P(1);
    Py = P(2);
    
    t = R*[1; 0];

    % signed distance
    d = t'*([x;y] - [Px;Py]);

end