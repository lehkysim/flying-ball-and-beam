classdef sys_obj_motor_pos_ctrl < matlab.System & coder.ExternalDependency ...

    properties
        % Simulink does not allow parameters whose values are booleans, literal character
        % vectors, string objects or non-numeric structures to be tunable. 
    end

    % Public, non-tunable properties
    properties(Nontunable)
        Port = '/dev/ttyACM0';
        dev_id = 1;
    end

    % Pre-computed constants
    properties(Access = private)
        position_offset = 0; % Because inner motor encoder is not 'aligned' with CUI encoders.
    end

    methods
        % -----------------------------------
        % ---------Constructor---------------
        % -----------------------------------
        function obj = sys_obj_motor_pos_ctrl(varargin)
            coder.allowpcode('plain');
            setProperties(obj,nargin,varargin{:});
        end
        % -----------------------------------
        % -----------------------------------

    end

    methods(Access = protected)

        % -----------------------------------
        % ---------Setup---------------------
        % -----------------------------------
        function setupImpl(obj)
            if isempty(coder.target)
                % Just do nothing?
            else
                coder.cinclude('MoteusAPI_ERT.h');
%                 coder.cinclude('moteus_protocol.h');
                coder.ceval('MoteusAPI_ERT_init', [obj.Port 0], int32(obj.dev_id));
                coder.ceval('readPosition', coder.wref(obj.position_offset)); 
            end
            % Perform one-time calculations, such as computing constants
        end

                
        % -----------------------------------
        % ---------Step----------------------
        % -----------------------------------
        function out_pos = stepImpl(obj, radPos)
            % Implement algorithm. Calculate y as a function of input u and
            % discrete states.
            if isempty(coder.target)
                % 
            else
%                 coder.ceval('setTorque', 0.1);
                offset = double(obj.position_offset);
                des_pos = double(-radPos/2/pi + offset(1));
                coder.ceval('setPosition', des_pos, 3, 0.01, 0);
            end
            curr_pos = 0;
            coder.ceval('readPosition', coder.wref(curr_pos)); 
            out_pos = -2*pi*(curr_pos - obj.position_offset);

        end

        function releaseImpl(obj)
            if isempty(coder.target)
                % Just do nothing?
            else
                % Call C-function implementing device termination
                coder.ceval('setStop');
                coder.ceval('MoteusAPI_Terminate');
            end
        end

        % ----------------------
        % ----------------------
        % ----------------------

        % ----------------------
        function num = getNumInputsImpl(~)
            num = 1;
        end
        
        function in = getInputNamesImpl(obj)
            in(1) = "Angle [rad]";
        end
        
        function in = getInputDataTypeImpl(obj)
            in(1) = 'double';
        end

        function in = getInputSizeImpl(obj)
            in(1) = 1; % Size of init message is always 7 (TODO)
        end


        % ----------------------
        function num = getOutputSizeImpl(obj)
            % Return size for each output port
            num = 1;
            % Example: inherit size from first input port
            % out = propagatedInputSize(obj,1);
        end

        % This function just needed to be here
        function flag = isOutputSizeLockedImpl(~,~)
            flag = true;
        end

        % ----------------------
        function out = getOutputDataTypeImpl(obj)
            out = 'double';
        end

        % ----------------------
        function out = isOutputFixedSizeImpl(obj,~)
            out = true;
        end

        % ----------------------
        function out = isOutputComplexImpl(obj)
            out = false;
        end

        %
        function out = getOutputNamesImpl(obj)
            out = "Motor angle [rad]";
        end

        function icon = getIconImpl(obj)
            % Define icon for System block
%             icon = mfilename("class"); % Use class name
            icon = ["Moteus", "Ctrl"]; % Example: text icon
            % icon = ["My","System"]; % Example: multi-line text icon
            % icon = matlab.system.display.Icon("myicon.jpg"); % Example: image file icon
        end

    end




    methods (Static, Access=protected)
        function simMode = getSimulateUsingImpl(~)
            simMode = 'Interpreted execution';
        end
        
        function isVisible = showSimulateUsingImpl
            isVisible = false;
        end
    end
    


    methods(Static)
        function name = getDescriptiveName()
            name = 'SMT';
        end
        
        function b = isSupportedContext(context)
            b = context.isCodeGenTarget('rtw');
        end

        function updateBuildInfo(buildInfo, context)
            if context.isCodeGenTarget('rtw')
                srcDir = fullfile(fileparts(mfilename('fullpath')),'cpp_moteusapi'); 
                includeDir = fullfile(fileparts(mfilename('fullpath')),'cpp_moteusapi');
                addIncludePaths(buildInfo,includeDir);

                addIncludeFiles(buildInfo,'moteus_protocol.h', includeDir);
                addIncludeFiles(buildInfo,'MoteusAPI_ERT.h', includeDir);
                addIncludeFiles(buildInfo,'MoteusAPI.h', includeDir);

                addSourceFiles(buildInfo,'MoteusAPI_ERT.cpp', srcDir);
                addSourceFiles(buildInfo,'MoteusAPI.cpp', srcDir);

                addCompileFlags(buildInfo,{'-std=c++14'});
            end
        end

    end
end
