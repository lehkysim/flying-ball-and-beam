
#include "MoteusAPI_ERT.h"
#include "MoteusAPI.h"
#include <memory>
#include <string>

std::unique_ptr<MoteusAPI> apiPtr;

void MoteusAPI_ERT_init(const char *port, int32_T moteus_id) {
    auto port_string = std::string(port);
    apiPtr = std::make_unique<MoteusAPI>(port_string, moteus_id);
}

void setTorque(real64_T torque) {
  // Maximal torque probably works only for position/speed control
  // Works only when the PID constants for position tracking are set to zero.
  apiPtr->SendPositionCommand(0, 0, 0, torque, 0, 0);
}

void readPosition(real64_T* position_out) {
  State curr_state;
  apiPtr->ReadState(curr_state.EN_Position());
  *position_out = curr_state.position;
}

void setPosition(real64_T* stop_position, real64_T velocity, real64_T max_torque, real64_T feedforward_torque) {
  // Maximal torque probably works only for position/speed control
  // Works only when the PID constants for position tracking are set to zero.

  // NOTE: one must set PID constants using Moteus API
  apiPtr->SendPositionCommand(*stop_position, velocity, max_torque, feedforward_torque);
}


void setStop() {
  apiPtr->SendStopCommand();
}

void MoteusAPI_Terminate() {
    if (apiPtr) {
        apiPtr.release();
    }
}
