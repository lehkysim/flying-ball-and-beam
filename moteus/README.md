# Moteus motor calibration

1) Connect a Moteus motor to your computer via a USB cable

2) Start ```sudo python3 -m moteus.moteus_tool --target 1 --calibrate```

3) Open tview ```sudo tview```

4) Remove limit on minimal and maximal motor position

    ```
    conf set servopos.position_min nan
    conf set servopos.position_max nan
    ```

5) Change constants of the motor pid regulator (optional)  
    ```
    conf set servo.pid_position.kp VALUE
    conf set servo.pid_position.kd VALUE
    conf set servo.pid_position.ki VALUE
    ```
    
6) Save the configuration ```conf write```

7) Test motor in positional mode ```d pos A B C sD``` 

    where
    * A = starting position
    * B = velocity
    * C = maximal torque
    * D = ending position
    
    e.g. ```d pos nan 0.5 0.3 s0.8```

8) Stop motor ```d stop```
