n = 12;

% image plane coords
x = zeros(2,n);
% world plane coords
X = zeros(2,n);

% X = H*x

% CAM: [0,0] in bottom right corner [px]
% BaB: [0,0] in beam center pivot   [mm]

% 6 angles:  {0, pi/3, 2*pi/3, pi, 4*pi/3, 5*pi/3} [rad]
% 2 lengths: {30,60} [mm]

%% POINT 1

i = 1;
d = 30; ang = 0;
ang = deg2rad(ang);

x(1,i) = 173.31;
x(2,i) = 219.57;
X(1,i) = d*cos(ang);
X(2,i) = d*sin(ang);

%% POINT 2

i = 2;
d = 30; ang = 60;
ang = deg2rad(ang);

x(1,i) = 202.86;
x(2,i) = 275.03;
X(1,i) = d*cos(ang);
X(2,i) = d*sin(ang);

%% POINT 3

i = 3;
d = 30; ang = 120;
ang = deg2rad(ang);

x(1,i) = 266.53;
x(2,i) = 274.65;
X(1,i) = d*cos(ang);
X(2,i) = d*sin(ang);

%% POINT 4

i = 4;
d = 30; ang = 180;
ang = deg2rad(ang);

x(1,i) = 297.82;
x(2,i) = 221.01;
X(1,i) = d*cos(ang);
X(2,i) = d*sin(ang);

%% POINT 5

i = 5;
d = 30; ang = 240;
ang = deg2rad(ang);

x(1,i) = 267.07;
x(2,i) = 167.06;
X(1,i) = d*cos(ang);
X(2,i) = d*sin(ang);

%% POINT 6

i = 6;
d = 30; ang = 300;
ang = deg2rad(ang);

x(1,i) = 204.03;
x(2,i) = 166.49;
X(1,i) = d*cos(ang);
X(2,i) = d*sin(ang);

%% POINT 7

i = 7;
d = 60; ang = 0;
ang = deg2rad(ang);

x(1,i) = 114.71;
x(2,i) = 218.37;
X(1,i) = d*cos(ang);
X(2,i) = d*sin(ang);

%% POINT 8

i = 8;
d = 60; ang = 60;
ang = deg2rad(ang);

x(1,i) = 173.51;
x(2,i) = 324.37;
X(1,i) = d*cos(ang);
X(2,i) = d*sin(ang);

%% POINT 9

i = 9;
d = 60; ang = 120;
ang = deg2rad(ang);

x(1,i) = 292.82;
x(2,i) = 327.86;
X(1,i) = d*cos(ang);
X(2,i) = d*sin(ang);

%% POINT 10

i = 10;
d = 60; ang = 180;
ang = deg2rad(ang);

x(1,i) = 357.41;
x(2,i) = 227.20;
X(1,i) = d*cos(ang);
X(2,i) = d*sin(ang);

%% POINT 11

i = 11;
d = 60; ang = 240;
ang = deg2rad(ang);

x(1,i) = 295.45;
x(2,i) = 114.83;
X(1,i) = d*cos(ang);
X(2,i) = d*sin(ang);

%% POINT 12

i = 12;
d = 60; ang = 300;
ang = deg2rad(ang);

x(1,i) = 176.81;
x(2,i) = 115.09;
X(1,i) = d*cos(ang);
X(2,i) = d*sin(ang);

%% SAVE TO .MAT

save("image_pts","x");
save("world_pts","X");
