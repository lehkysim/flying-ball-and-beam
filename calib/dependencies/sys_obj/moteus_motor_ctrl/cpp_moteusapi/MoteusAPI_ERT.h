
#ifndef MOTEUSAPI_ERT_H__
#define MOTEUSAPI_ERT_H__


#include "rtwtypes.h"

// ------------------------
#ifdef __cplusplus
extern "C" {
#endif
// ------------------------


void MoteusAPI_ERT_init(const char *port, int32_T moteus_id);

void setTorque(real64_T torque);

void setPosition(real64_T stop_position, real64_T velocity, real64_T max_torque, real64_T feedforward_torque);

void readPosition(real64_T* position_out);

void setStop();

void MoteusAPI_Terminate();

// ------------------------
#ifdef __cplusplus
}
#endif
// ------------------------




#endif