load("image_pts.mat");
load("world_pts.mat");

% image plane
graph1 = plot(x(1,:), x(2,:), "x");
set(graph1, 'LineWidth', 5);
xlim([0 480]);
ylim([0 480]);
title('Image plane')

% world plane
figure;
graph2 = plot(X(1,:), X(2,:), "x");
set(graph2, 'LineWidth', 5);
xlim([-110 110]);
ylim([-110 110]);
title('World plane')
