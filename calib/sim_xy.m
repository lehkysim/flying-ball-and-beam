clc;

x = xout.signals.values;
y = yout.signals.values;

x_med = median(x);
y_med = median(y);

disp(round(x_med,2));
disp(round(y_med,2));
