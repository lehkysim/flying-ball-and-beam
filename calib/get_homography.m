addpath('../raspi-ballpos/matlabFunctions');

load("image_pts.mat");
load("world_pts.mat");

% get homography H
H = hom_calib(x, X);
H = H/H(3,3);

save("H.mat","H");
