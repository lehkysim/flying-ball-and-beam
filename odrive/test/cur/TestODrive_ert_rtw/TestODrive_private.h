/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * File: TestODrive_private.h
 *
 * Code generated for Simulink model 'TestODrive'.
 *
 * Model version                  : 8.35
 * Simulink Coder version         : 9.6 (R2021b) 14-May-2021
 * C/C++ source code generated on : Mon Sep 12 14:53:38 2022
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: ARM Compatible->ARM Cortex
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_TestODrive_private_h_
#define RTW_HEADER_TestODrive_private_h_
#include "rtwtypes.h"
#endif                                 /* RTW_HEADER_TestODrive_private_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
