/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * File: TestODrive_data.c
 *
 * Code generated for Simulink model 'TestODrive'.
 *
 * Model version                  : 8.35
 * Simulink Coder version         : 9.6 (R2021b) 14-May-2021
 * C/C++ source code generated on : Mon Sep 12 14:53:38 2022
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: ARM Compatible->ARM Cortex
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "TestODrive.h"
#include "TestODrive_private.h"

/* Block parameters (default storage) */
P_TestODrive_T TestODrive_P = {
  /* Mask Parameter: PIDController_P
   * Referenced by: '<S38>/Proportional Gain'
   */
  1.0,

  /* Expression: 0.0001
   * Referenced by: '<Root>/Constant'
   */
  0.0001,

  /* Expression: 0.0
   * Referenced by: '<Root>/Delay'
   */
  0.0,

  /* Expression: 1
   * Referenced by: '<Root>/Gain'
   */
  1.0
};

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
