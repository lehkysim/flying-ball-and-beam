/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * File: TestODrive.h
 *
 * Code generated for Simulink model 'TestODrive'.
 *
 * Model version                  : 8.35
 * Simulink Coder version         : 9.6 (R2021b) 14-May-2021
 * C/C++ source code generated on : Mon Sep 12 14:53:38 2022
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: ARM Compatible->ARM Cortex
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_TestODrive_h_
#define RTW_HEADER_TestODrive_h_
#include <stddef.h>
#include <string.h>
#ifndef TestODrive_COMMON_INCLUDES_
#define TestODrive_COMMON_INCLUDES_
#include "rtwtypes.h"
#include "odrive.h"
#endif                                 /* TestODrive_COMMON_INCLUDES_ */

#include "TestODrive_types.h"

/* Macros for accessing real-time model data structure */
#ifndef rtmGetErrorStatus
#define rtmGetErrorStatus(rtm)         ((rtm)->errorStatus)
#endif

#ifndef rtmSetErrorStatus
#define rtmSetErrorStatus(rtm, val)    ((rtm)->errorStatus = (val))
#endif

/* Block signals (default storage) */
typedef struct {
  h_cell_TestODrive_T a__3;
  char_T t[48];
  char_T s[42];
  char_T d[40];
  char_T p[39];
  char_T v[37];
  char_T m[34];
  char_T k[31];
  char_T b[27];
  char_T n[26];
  g_cell_TestODrive_T a__1;
  char_T i[23];
  char_T u[22];
  char_T h[20];
} B_TestODrive_T;

/* Block states (default storage) for system '<Root>' */
typedef struct {
  ODrive_TestODrive_T obj;             /* '<Root>/MATLAB System' */
  real_T Delay_DSTATE;                 /* '<Root>/Delay' */
} DW_TestODrive_T;

/* Parameters (default storage) */
struct P_TestODrive_T_ {
  real_T PIDController_P;              /* Mask Parameter: PIDController_P
                                        * Referenced by: '<S38>/Proportional Gain'
                                        */
  real_T Constant_Value;               /* Expression: 0.0001
                                        * Referenced by: '<Root>/Constant'
                                        */
  real_T Delay_InitialCondition;       /* Expression: 0.0
                                        * Referenced by: '<Root>/Delay'
                                        */
  real_T Gain_Gain;                    /* Expression: 1
                                        * Referenced by: '<Root>/Gain'
                                        */
};

/* Real-time Model Data Structure */
struct tag_RTM_TestODrive_T {
  const char_T * volatile errorStatus;
};

/* Block parameters (default storage) */
extern P_TestODrive_T TestODrive_P;

/* Block signals (default storage) */
extern B_TestODrive_T TestODrive_B;

/* Block states (default storage) */
extern DW_TestODrive_T TestODrive_DW;

/* Model entry point functions */
extern void TestODrive_initialize(void);
extern void TestODrive_step(void);
extern void TestODrive_terminate(void);

/* Real-time Model object */
extern RT_MODEL_TestODrive_T *const TestODrive_M;

/*-
 * These blocks were eliminated from the model due to optimizations:
 *
 * Block '<Root>/Scope' : Unused code path elimination
 */

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'TestODrive'
 * '<S1>'   : 'TestODrive/PID Controller'
 * '<S2>'   : 'TestODrive/PID Controller/Anti-windup'
 * '<S3>'   : 'TestODrive/PID Controller/D Gain'
 * '<S4>'   : 'TestODrive/PID Controller/Filter'
 * '<S5>'   : 'TestODrive/PID Controller/Filter ICs'
 * '<S6>'   : 'TestODrive/PID Controller/I Gain'
 * '<S7>'   : 'TestODrive/PID Controller/Ideal P Gain'
 * '<S8>'   : 'TestODrive/PID Controller/Ideal P Gain Fdbk'
 * '<S9>'   : 'TestODrive/PID Controller/Integrator'
 * '<S10>'  : 'TestODrive/PID Controller/Integrator ICs'
 * '<S11>'  : 'TestODrive/PID Controller/N Copy'
 * '<S12>'  : 'TestODrive/PID Controller/N Gain'
 * '<S13>'  : 'TestODrive/PID Controller/P Copy'
 * '<S14>'  : 'TestODrive/PID Controller/Parallel P Gain'
 * '<S15>'  : 'TestODrive/PID Controller/Reset Signal'
 * '<S16>'  : 'TestODrive/PID Controller/Saturation'
 * '<S17>'  : 'TestODrive/PID Controller/Saturation Fdbk'
 * '<S18>'  : 'TestODrive/PID Controller/Sum'
 * '<S19>'  : 'TestODrive/PID Controller/Sum Fdbk'
 * '<S20>'  : 'TestODrive/PID Controller/Tracking Mode'
 * '<S21>'  : 'TestODrive/PID Controller/Tracking Mode Sum'
 * '<S22>'  : 'TestODrive/PID Controller/Tsamp - Integral'
 * '<S23>'  : 'TestODrive/PID Controller/Tsamp - Ngain'
 * '<S24>'  : 'TestODrive/PID Controller/postSat Signal'
 * '<S25>'  : 'TestODrive/PID Controller/preSat Signal'
 * '<S26>'  : 'TestODrive/PID Controller/Anti-windup/Disabled'
 * '<S27>'  : 'TestODrive/PID Controller/D Gain/Disabled'
 * '<S28>'  : 'TestODrive/PID Controller/Filter/Disabled'
 * '<S29>'  : 'TestODrive/PID Controller/Filter ICs/Disabled'
 * '<S30>'  : 'TestODrive/PID Controller/I Gain/Disabled'
 * '<S31>'  : 'TestODrive/PID Controller/Ideal P Gain/Passthrough'
 * '<S32>'  : 'TestODrive/PID Controller/Ideal P Gain Fdbk/Disabled'
 * '<S33>'  : 'TestODrive/PID Controller/Integrator/Disabled'
 * '<S34>'  : 'TestODrive/PID Controller/Integrator ICs/Disabled'
 * '<S35>'  : 'TestODrive/PID Controller/N Copy/Disabled wSignal Specification'
 * '<S36>'  : 'TestODrive/PID Controller/N Gain/Disabled'
 * '<S37>'  : 'TestODrive/PID Controller/P Copy/Disabled'
 * '<S38>'  : 'TestODrive/PID Controller/Parallel P Gain/Internal Parameters'
 * '<S39>'  : 'TestODrive/PID Controller/Reset Signal/Disabled'
 * '<S40>'  : 'TestODrive/PID Controller/Saturation/Passthrough'
 * '<S41>'  : 'TestODrive/PID Controller/Saturation Fdbk/Disabled'
 * '<S42>'  : 'TestODrive/PID Controller/Sum/Passthrough_P'
 * '<S43>'  : 'TestODrive/PID Controller/Sum Fdbk/Disabled'
 * '<S44>'  : 'TestODrive/PID Controller/Tracking Mode/Disabled'
 * '<S45>'  : 'TestODrive/PID Controller/Tracking Mode Sum/Passthrough'
 * '<S46>'  : 'TestODrive/PID Controller/Tsamp - Integral/Disabled wSignal Specification'
 * '<S47>'  : 'TestODrive/PID Controller/Tsamp - Ngain/Passthrough'
 * '<S48>'  : 'TestODrive/PID Controller/postSat Signal/Forward_Path'
 * '<S49>'  : 'TestODrive/PID Controller/preSat Signal/Forward_Path'
 */
#endif                                 /* RTW_HEADER_TestODrive_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
