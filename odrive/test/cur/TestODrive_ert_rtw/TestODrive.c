/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * File: TestODrive.c
 *
 * Code generated for Simulink model 'TestODrive'.
 *
 * Model version                  : 8.35
 * Simulink Coder version         : 9.6 (R2021b) 14-May-2021
 * C/C++ source code generated on : Mon Sep 12 14:53:38 2022
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: ARM Compatible->ARM Cortex
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "TestODrive.h"
#include "TestODrive_private.h"

/* Block signals (default storage) */
B_TestODrive_T TestODrive_B;

/* Block states (default storage) */
DW_TestODrive_T TestODrive_DW;

/* Real-time model */
static RT_MODEL_TestODrive_T TestODrive_M_;
RT_MODEL_TestODrive_T *const TestODrive_M = &TestODrive_M_;

/* Forward declaration for local functions */
static void TestODriv_ODrive_generateInputs(g_cell_TestODrive_T *names,
  cell_TestODrive_T *parameters, real_T multipliers[20]);
static void TestODri_ODrive_generateOutputs(h_cell_TestODrive_T *names,
  b_cell_TestODrive_T *parameters, real_T multipliers[20]);
static void TestODrive_ODrive_setupImpl(ODrive_TestODrive_T *obj);
static void TestODriv_ODrive_generateInputs(g_cell_TestODrive_T *names,
  cell_TestODrive_T *parameters, real_T multipliers[20])
{
  int32_T i;
  static const char_T tmp[17] = { 'A', 'x', 'i', 's', ' ', '0', ' ', 'R', 'e',
    'f', 'e', 'r', 'e', 'n', 'c', 'e', ' ' };

  static const char_T tmp_0[8] = { 'v', 'e', 'l', 'o', 'c', 'i', 't', 'y' };

  for (i = 0; i < 20; i++) {
    multipliers[i] = 1.0;
  }

  for (i = 0; i < 17; i++) {
    names->f1[i] = tmp[i];
  }

  for (i = 0; i < 8; i++) {
    names->f1[i + 17] = tmp_0[i];
  }

  parameters->f1 = 'v';

  /*                    multipliers(count) = obj.CountsPerRotate0/(2*pi); */
  multipliers[0] = 0.15915494309189535;
}

static void TestODri_ODrive_generateOutputs(h_cell_TestODrive_T *names,
  b_cell_TestODrive_T *parameters, real_T multipliers[20])
{
  int32_T i;
  static const char_T tmp[26] = { 'a', 'x', 'i', 's', '0', '.', 'e', 'n', 'c',
    'o', 'd', 'e', 'r', '.', 'p', 'o', 's', '_', 'e', 's', 't', 'i', 'm', 'a',
    't', 'e' };

  static const char_T tmp_0[31] = { 'A', 'x', 'i', 's', ' ', '0', ' ', 'E', 's',
    't', 'i', 'm', 'a', 't', 'e', 'd', ' ', 'p', 'o', 's', 'i', 't', 'i', 'o',
    'n', ' ', '[', 'r', 'a', 'd', ']' };

  static const char_T tmp_1[26] = { 'a', 'x', 'i', 's', '0', '.', 'e', 'n', 'c',
    'o', 'd', 'e', 'r', '.', 'v', 'e', 'l', '_', 'e', 's', 't', 'i', 'm', 'a',
    't', 'e' };

  static const char_T tmp_2[33] = { 'A', 'x', 'i', 's', ' ', '0', ' ', 'E', 's',
    't', 'i', 'm', 'a', 't', 'e', 'd', ' ', 'v', 'e', 'l', 'o', 'c', 'i', 't',
    'y', ' ', '[', 'r', 'a', 'd', '/', 's', ']' };

  static const char_T tmp_3[39] = { 'a', 'x', 'i', 's', '0', '.', 'm', 'o', 't',
    'o', 'r', '.', 'c', 'u', 'r', 'r', 'e', 'n', 't', '_', 'c', 'o', 'n', 't',
    'r', 'o', 'l', '.', 'I', 'q', '_', 'm', 'e', 'a', 's', 'u', 'r', 'e', 'd' };

  static const char_T tmp_4[27] = { 'A', 'x', 'i', 's', ' ', '0', ' ', 'M', 'e',
    'a', 's', 'u', 'r', 'e', 'd', ' ', 'c', 'u', 'r', 'r', 'e', 'n', 't', ' ',
    '[', 'A', ']' };

  static const char_T tmp_5[11] = { 'a', 'x', 'i', 's', '0', '.', 'e', 'r', 'r',
    'o', 'r' };

  static const char_T tmp_6[18] = { 'A', 'x', 'i', 's', ' ', '0', ' ', 'E', 'r',
    'r', 'o', 'r', ' ', 's', 't', 'a', 't', 'e' };

  static const char_T tmp_7[12] = { 'v', 'b', 'u', 's', '_', 'v', 'o', 'l', 't',
    'a', 'g', 'e' };

  static const char_T tmp_8[15] = { 'B', 'u', 's', ' ', 'v', 'o', 'l', 't', 'a',
    'g', 'e', ' ', '[', 'V', ']' };

  for (i = 0; i < 20; i++) {
    multipliers[i] = 1.0;
  }

  for (i = 0; i < 26; i++) {
    parameters->f1[i] = tmp[i];
  }

  for (i = 0; i < 31; i++) {
    names->f1[i] = tmp_0[i];
  }

  /*                    multipliers(count) = (2*pi)/obj.CountsPerRotate0; */
  multipliers[0] = 6.2831853071795862;
  for (i = 0; i < 26; i++) {
    parameters->f2[i] = tmp_1[i];
  }

  for (i = 0; i < 33; i++) {
    names->f2[i] = tmp_2[i];
  }

  /*                    multipliers(count) = (2*pi)/obj.CountsPerRotate0; */
  multipliers[1] = 6.2831853071795862;
  for (i = 0; i < 39; i++) {
    parameters->f3[i] = tmp_3[i];
  }

  for (i = 0; i < 27; i++) {
    names->f3[i] = tmp_4[i];
  }

  for (i = 0; i < 11; i++) {
    parameters->f4[i] = tmp_5[i];
  }

  for (i = 0; i < 18; i++) {
    names->f4[i] = tmp_6[i];
  }

  for (i = 0; i < 12; i++) {
    parameters->f5[i] = tmp_7[i];
  }

  for (i = 0; i < 15; i++) {
    names->f5[i] = tmp_8[i];
  }
}

static void TestODrive_ODrive_setupImpl(ODrive_TestODrive_T *obj)
{
  int32_T i;
  char_T g[18];
  char_T j[12];
  char_T f[11];
  boolean_T encoder0_ready;
  boolean_T motor0_calibrated;
  static const char_T tmp[11] = "/dev/ttyS0";
  static const char_T tmp_0[18] = "axis0.motor.error";
  static const char_T tmp_1[20] = "axis0.encoder.error";
  static const char_T tmp_2[23] = "axis0.controller.error";
  static const char_T tmp_3[12] = "axis0.error";
  static const char_T tmp_4[31] = "axis0.encoder.config.use_index";
  static const char_T tmp_5[31] = "axis0.motor.config.current_lim";
  static const char_T tmp_6[34] = "axis0.controller.config.vel_limit";
  static const char_T tmp_7[26] = "axis0.motor.is_calibrated";
  static const char_T tmp_8[23] = "axis0.encoder.is_ready";
  static const char_T tmp_9[39] = "axis0.config.startup_motor_calibration";
  static const char_T tmp_a[42] = "axis0.config.startup_encoder_index_search";
  static const char_T tmp_b[48] =
    "axis0.config.startup_encoder_offset_calibration";
  static const char_T tmp_c[22] = "axis0.requested_state";
  static const char_T tmp_d[37] = "axis0.controller.config.control_mode";
  obj->portFilePointer = 0;
  TestODriv_ODrive_generateInputs(&TestODrive_B.a__1, &obj->inputParameters,
    obj->inputMultiplier);
  TestODri_ODrive_generateOutputs(&TestODrive_B.a__3, &obj->outputParameters,
    obj->outputMultiplier);

  /*  Call C-function implementing device initialization */
  for (i = 0; i < 11; i++) {
    f[i] = tmp[i];
  }

  obj->portFilePointer = odrive_open_port(&f[0], 115200);

  /*  Reset all error codes */
  for (i = 0; i < 18; i++) {
    g[i] = tmp_0[i];
  }

  odrive_write_int(obj->portFilePointer, &g[0], 0);
  for (i = 0; i < 20; i++) {
    TestODrive_B.h[i] = tmp_1[i];
  }

  odrive_write_int(obj->portFilePointer, &TestODrive_B.h[0], 0);
  for (i = 0; i < 23; i++) {
    TestODrive_B.i[i] = tmp_2[i];
  }

  odrive_write_int(obj->portFilePointer, &TestODrive_B.i[0], 0);
  for (i = 0; i < 12; i++) {
    j[i] = tmp_3[i];
  }

  odrive_write_int(obj->portFilePointer, &j[0], 0);
  for (i = 0; i < 31; i++) {
    TestODrive_B.k[i] = tmp_4[i];
  }

  odrive_write_int(obj->portFilePointer, &TestODrive_B.k[0], 0);
  for (i = 0; i < 31; i++) {
    TestODrive_B.k[i] = tmp_5[i];
  }

  odrive_write_float(obj->portFilePointer, &TestODrive_B.k[0], 10.0);

  /*                    vel_limit = (obj.VelocityLimit0*obj.CountsPerRotate0)/(2*pi); */
  for (i = 0; i < 34; i++) {
    TestODrive_B.m[i] = tmp_6[i];
  }

  odrive_write_float(obj->portFilePointer, &TestODrive_B.m[0], 10.0);
  for (i = 0; i < 26; i++) {
    TestODrive_B.n[i] = tmp_7[i];
  }

  motor0_calibrated = odrive_read_int(obj->portFilePointer, &TestODrive_B.n[0]);
  for (i = 0; i < 23; i++) {
    TestODrive_B.i[i] = tmp_8[i];
  }

  encoder0_ready = odrive_read_int(obj->portFilePointer, &TestODrive_B.i[0]);
  for (i = 0; i < 39; i++) {
    TestODrive_B.p[i] = tmp_9[i];
  }

  odrive_write_int(obj->portFilePointer, &TestODrive_B.p[0], !motor0_calibrated);
  if (!encoder0_ready) {
    for (i = 0; i < 42; i++) {
      TestODrive_B.s[i] = tmp_a[i];
    }

    odrive_write_int(obj->portFilePointer, &TestODrive_B.s[0], false);
    for (i = 0; i < 48; i++) {
      TestODrive_B.t[i] = tmp_b[i];
    }

    odrive_write_int(obj->portFilePointer, &TestODrive_B.t[0], true);
  } else {
    for (i = 0; i < 42; i++) {
      TestODrive_B.s[i] = tmp_a[i];
    }

    odrive_write_int(obj->portFilePointer, &TestODrive_B.s[0], false);
    for (i = 0; i < 48; i++) {
      TestODrive_B.t[i] = tmp_b[i];
    }

    odrive_write_int(obj->portFilePointer, &TestODrive_B.t[0], false);
  }

  for (i = 0; i < 22; i++) {
    TestODrive_B.u[i] = tmp_c[i];
  }

  odrive_write_int(obj->portFilePointer, &TestODrive_B.u[0], 2);
  odrive_wait_for_state(obj->portFilePointer, 0, 1, 100000, 0);
  for (i = 0; i < 37; i++) {
    TestODrive_B.v[i] = tmp_d[i];
  }

  odrive_write_int(obj->portFilePointer, &TestODrive_B.v[0], 2);
  for (i = 0; i < 22; i++) {
    TestODrive_B.u[i] = tmp_c[i];
  }

  odrive_write_int(obj->portFilePointer, &TestODrive_B.u[0], 8);
}

/* Model step function */
void TestODrive_step(void)
{
  real_T value;
  int32_T i;
  char_T f[13];
  char_T e[12];
  uint8_T tmp;

  /* MATLABSystem: '<Root>/MATLAB System' incorporates:
   *  Constant: '<Root>/Constant'
   *  Delay: '<Root>/Delay'
   *  Gain: '<Root>/Gain'
   *  Gain: '<S38>/Proportional Gain'
   *  Sum: '<Root>/Sum'
   */
  /*         %% Define input properties */
  tmp = (uint8_T)TestODrive_DW.obj.inputParameters.f1;
  if ((uint8_T)TestODrive_DW.obj.inputParameters.f1 > 127) {
    tmp = 127U;
  }

  odrive_quick_write(TestODrive_DW.obj.portFilePointer, (int8_T)tmp, 0,
                     TestODrive_P.Gain_Gain * (TestODrive_P.PIDController_P *
    (TestODrive_P.Constant_Value - TestODrive_DW.Delay_DSTATE)) *
                     TestODrive_DW.obj.inputMultiplier[0]);
  for (i = 0; i < 26; i++) {
    TestODrive_B.b[i] = TestODrive_DW.obj.outputParameters.f1[i];
  }

  TestODrive_B.b[26] = '\x00';
  odrive_read_float(TestODrive_DW.obj.portFilePointer, &TestODrive_B.b[0]);
  for (i = 0; i < 26; i++) {
    TestODrive_B.b[i] = TestODrive_DW.obj.outputParameters.f2[i];
  }

  TestODrive_B.b[26] = '\x00';
  value = odrive_read_float(TestODrive_DW.obj.portFilePointer, &TestODrive_B.b[0]);
  for (i = 0; i < 39; i++) {
    TestODrive_B.d[i] = TestODrive_DW.obj.outputParameters.f3[i];
  }

  TestODrive_B.d[39] = '\x00';
  odrive_read_float(TestODrive_DW.obj.portFilePointer, &TestODrive_B.d[0]);
  for (i = 0; i < 11; i++) {
    e[i] = TestODrive_DW.obj.outputParameters.f4[i];
  }

  e[11] = '\x00';
  odrive_read_float(TestODrive_DW.obj.portFilePointer, &e[0]);
  for (i = 0; i < 12; i++) {
    f[i] = TestODrive_DW.obj.outputParameters.f5[i];
  }

  f[12] = '\x00';
  odrive_read_float(TestODrive_DW.obj.portFilePointer, &f[0]);
  TestODrive_DW.Delay_DSTATE = value * TestODrive_DW.obj.outputMultiplier[1];

  /* End of MATLABSystem: '<Root>/MATLAB System' */
}

/* Model initialize function */
void TestODrive_initialize(void)
{
  /* Registration code */

  /* initialize error status */
  rtmSetErrorStatus(TestODrive_M, (NULL));

  /* states (dwork) */
  (void) memset((void *)&TestODrive_DW, 0,
                sizeof(DW_TestODrive_T));

  /* InitializeConditions for Delay: '<Root>/Delay' */
  TestODrive_DW.Delay_DSTATE = TestODrive_P.Delay_InitialCondition;

  /* Start for MATLABSystem: '<Root>/MATLAB System' */
  TestODrive_DW.obj.matlabCodegenIsDeleted = false;
  TestODrive_DW.obj.isSetupComplete = false;
  TestODrive_DW.obj.isInitialized = 1;

  /*         %% Define input properties */
  TestODrive_ODrive_setupImpl(&TestODrive_DW.obj);
  TestODrive_DW.obj.isSetupComplete = true;
}

/* Model terminate function */
void TestODrive_terminate(void)
{
  int32_T i;
  char_T b[22];
  static const char_T tmp[22] = "axis0.requested_state";

  /* Terminate for MATLABSystem: '<Root>/MATLAB System' */
  if (!TestODrive_DW.obj.matlabCodegenIsDeleted) {
    TestODrive_DW.obj.matlabCodegenIsDeleted = true;
    if ((TestODrive_DW.obj.isInitialized == 1) &&
        TestODrive_DW.obj.isSetupComplete) {
      /*  Call C-function implementing device termination */
      for (i = 0; i < 22; i++) {
        b[i] = tmp[i];
      }

      odrive_write_int(TestODrive_DW.obj.portFilePointer, &b[0], 1);
    }
  }

  /* End of Terminate for MATLABSystem: '<Root>/MATLAB System' */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
