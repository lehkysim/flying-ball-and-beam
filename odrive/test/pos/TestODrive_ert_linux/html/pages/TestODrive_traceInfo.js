function RTW_rtwnameSIDMap() {
	this.rtwnameHashMap = new Array();
	this.sidHashMap = new Array();
	this.rtwnameHashMap["<Root>"] = {sid: "TestODrive"};
	this.sidHashMap["TestODrive"] = {rtwname: "<Root>"};
	this.rtwnameHashMap["<Root>/BuildForRemoteTarget_armhf"] = {sid: "TestODrive:11"};
	this.sidHashMap["TestODrive:11"] = {rtwname: "<Root>/BuildForRemoteTarget_armhf"};
	this.rtwnameHashMap["<Root>/BuildParamsRemote_armhf"] = {sid: "TestODrive:3"};
	this.sidHashMap["TestODrive:3"] = {rtwname: "<Root>/BuildParamsRemote_armhf"};
	this.rtwnameHashMap["<Root>/Deploy_aarch64"] = {sid: "TestODrive:10"};
	this.sidHashMap["TestODrive:10"] = {rtwname: "<Root>/Deploy_aarch64"};
	this.rtwnameHashMap["<Root>/MATLAB System"] = {sid: "TestODrive:24"};
	this.sidHashMap["TestODrive:24"] = {rtwname: "<Root>/MATLAB System"};
	this.rtwnameHashMap["<Root>/Pulse Generator"] = {sid: "TestODrive:25"};
	this.sidHashMap["TestODrive:25"] = {rtwname: "<Root>/Pulse Generator"};
	this.rtwnameHashMap["<Root>/Scope"] = {sid: "TestODrive:26"};
	this.sidHashMap["TestODrive:26"] = {rtwname: "<Root>/Scope"};
	this.rtwnameHashMap["<Root>/TargetLoginParams_armhf"] = {sid: "TestODrive:13"};
	this.sidHashMap["TestODrive:13"] = {rtwname: "<Root>/TargetLoginParams_armhf"};
	this.getSID = function(rtwname) { return this.rtwnameHashMap[rtwname];}
	this.getRtwname = function(sid) { return this.sidHashMap[sid];}
}
RTW_rtwnameSIDMap.instance = new RTW_rtwnameSIDMap();
