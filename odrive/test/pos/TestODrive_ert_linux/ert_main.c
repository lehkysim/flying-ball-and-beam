/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * File: ert_main.c
 *
 * Code generated for Simulink model 'TestODrive'.
 *
 * Model version                  : 6.6
 * Simulink Coder version         : 9.7 (R2022a) 13-Nov-2021
 * C/C++ source code generated on : Mon Feb 13 12:19:25 2023
 *
 * Target selection: ert_linux.tlc
 * Embedded hardware selection: Intel->x86-64 (Windows64)
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

/* Multirate - Multitasking case main file */
#define _BSD_SOURCE                                              /* For usleep() */
#define _POSIX_C_SOURCE                200112L                   /* For clock_gettime() & clock_nanosleep() */
#include <stdio.h>              /* This ert_main.c example uses printf/fflush */
#include <pthread.h>                   /* Thread library header file */
#include <sched.h>                     /* OS scheduler header file */
#include <semaphore.h>                 /* Semaphores library header file */
#include <time.h>
#include <stdlib.h>
#include <stdbool.h>
#include <unistd.h>
#include <sys/mman.h>                  /* For mlockall() */
#include <signal.h>
#include "TestODrive.h"                /* Model's header file */
#include "rtwtypes.h"                  /* MathWorks types */
#include "ext_work.h"                  /* External mode header file */
#ifndef TRUE
#define TRUE                           true
#define FALSE                          false
#endif

/*==================*
 * Required defines *
 *==================*/
#ifndef MODEL
# error Must specify a model name. Define MODEL=name.
#else

/* create generic macros that work with any model */
# define EXPAND_CONCAT(name1,name2)    name1 ## name2
# define CONCAT(name1,name2)           EXPAND_CONCAT(name1,name2)
# define MODEL_INITIALIZE              CONCAT(MODEL,_initialize)
# define MODEL_STEP                    CONCAT(MODEL,_step)
# define MODEL_TERMINATE               CONCAT(MODEL,_terminate)
# define RT_MDL                        CONCAT(MODEL,_M)
#endif

/* Error checking */
#define STRINGIZE(num)                 #num
#define POS(line)                      __FILE__ ":" STRINGIZE(line)
#define CHECK0(expr)                   do { int __err = (expr); if (__err) { fprintf(stderr, "Error: %s returned '%s' at " POS(__LINE__) "\n", #expr, strerror(__err)); exit(1); } } while (0);
#define CHECKE(expr)                   do { if ((expr) == -1) { perror(#expr " at " POS(__LINE__)); exit(1); } } while (0);

/**
 * Maximal priority used by base rate thread.
 */
#define MAX_PRIO                       (sched_get_priority_min(SCHED_FIFO) + 1)

/**
 * Thread handle of the base rate thread.
 * Fundamental sample time = 2.5s
 */
pthread_t base_rate_thread;

/**
 * Thread handles of and semaphores for sub rate threads. The array
 * is indexed by TID, i.e. the first one or two elements are unused.
 */
struct sub_rate {
  pthread_t thread;
  sem_t sem;
} sub_rate[1];

/**
 * Flag if the simulation has been terminated.
 */
int simulationFinished = 0;

/* Indication that the base rate thread has started */
sem_t ext_mode_ready;

/**
 * This is the thread function of the base rate loop.
 * Fundamental sample time = 2.5s
 */
void * base_rate(void *param_unused)
{
  struct timespec now, next;
  struct timespec period = { 3U, 500000000U };/* 2.5 seconds */

  boolean_T eventFlags[1];             /* Model has 1 rates */
  int_T taskCounter[1] = { 0 };

  int_T OverrunFlags[1];
  int step_sem_value;
  int_T i;
  (void)param_unused;

  /* External mode */
  rtSetTFinalForExtMode(&rtmGetTFinal(TestODrive_M));
  rtExtModeCheckInit(1);

  {
    boolean_T rtmStopReq = false;
    rtExtModeWaitForStartPkt(TestODrive_M->extModeInfo, 1, &rtmStopReq);
    if (rtmStopReq) {
      rtmSetStopRequested(TestODrive_M, true);
    }
  }

  rtERTExtModeStartMsg();
  CHECKE(sem_post(&ext_mode_ready));
  clock_gettime(CLOCK_MONOTONIC, &next);

  /* Main loop, running until all the threads are terminated */
  while (rtmGetErrorStatus(TestODrive_M) == NULL && !rtmGetStopRequested
         (TestODrive_M)) {
    /* Check subrate overrun, set rates that need to run this time step*/

    /* Trigger sub-rate threads */

    /* Execute base rate step */
    TestODrive_step();
    rtExtModeCheckEndTrigger();
    do {
      next.tv_sec += period.tv_sec;
      next.tv_nsec += period.tv_nsec;
      if (next.tv_nsec >= 1000000000) {
        next.tv_sec++;
        next.tv_nsec -= 1000000000;
      }

      clock_gettime(CLOCK_MONOTONIC, &now);
      if (now.tv_sec > next.tv_sec ||
          (now.tv_sec == next.tv_sec && now.tv_nsec > next.tv_nsec)) {
        uint32_T usec = (now.tv_sec - next.tv_sec) * 1000000 + (now.tv_nsec -
          next.tv_nsec)/1000;
        fprintf(stderr, "Base rate (2.5s) overrun by %d us\n", usec);
        next = now;
        continue;
      }
    } while (0);

    clock_nanosleep(CLOCK_MONOTONIC, TIMER_ABSTIME, &next, NULL);
  }

  simulationFinished = 1;

  /* Final step */
  for (i = 1; i < 1; i++) {
    sem_post(&sub_rate[i].sem);
    sem_post(&sub_rate[i].sem);
  }

  return NULL;
}

/**
 * Signal handler for ABORT during simulation
 */
void abort_signal_handler(int sig)
{
  fprintf(stderr, "Simulation aborted by pressing CTRL+C\n");
  rtmSetStopRequested(TestODrive_M, 1);
}

/**
 * This is the main function of the model.
 * Multirate - Multitasking case main file
 */
int_T main(int_T argc, const char_T *argv[])
{
  const char_T *errStatus;
  int_T i;
  pthread_attr_t attr;
  struct sched_param sched_param;

  /* External mode */
  /* rtERTExtModeParseArgs(argc, argv); */
  rtExtModeParseArgs(argc, argv, NULL);
  CHECKE(sem_init(&ext_mode_ready, 0, 0));

#ifndef WITHOUT_MLOCK

  CHECKE(mlockall(MCL_CURRENT | MCL_FUTURE));

#endif

  /* Initialize model */
  TestODrive_initialize();
  simulationFinished = 0;

  /* Prepare task attributes */
  CHECK0(pthread_attr_init(&attr));
  CHECK0(pthread_attr_setinheritsched(&attr, PTHREAD_EXPLICIT_SCHED));
  CHECK0(pthread_attr_setschedpolicy(&attr, SCHED_FIFO));

  /* Starting the base rate thread */
  sched_param.sched_priority = MAX_PRIO;
  CHECK0(pthread_attr_setschedparam(&attr, &sched_param));
  CHECK0(pthread_create(&base_rate_thread, &attr, base_rate, NULL));
  CHECK0(pthread_attr_destroy(&attr));

  /* External mode */
  CHECKE(sem_wait(&ext_mode_ready));
  signal(SIGINT, abort_signal_handler);
                           /* important for letting the destructor be called. */
  while (rtmGetErrorStatus(TestODrive_M) == NULL && !rtmGetStopRequested
         (TestODrive_M)) {
    rtExtModeOneStep(rtmGetRTWExtModeInfo(RT_MDL), NUMST, (boolean_T *)
                     &rtmGetStopRequested(RT_MDL));
    usleep(2500000U);
  }

  /* Wait for threads to finish */
  pthread_join(base_rate_thread, NULL);
  rtExtModeShutdown(1);

  /* Terminate model */
  TestODrive_terminate();
  errStatus = rtmGetErrorStatus(TestODrive_M);
  if (errStatus != NULL && strcmp(errStatus, "Simulation finished")) {
    if (!strcmp(errStatus, "Overrun")) {
      printf("ISR overrun - sampling rate too fast\n");
    }

    return(1);
  }

  return 0;
}

/* Local Variables: */
/* compile-command: "make -f TestODrive.mk" */
/* End: */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
