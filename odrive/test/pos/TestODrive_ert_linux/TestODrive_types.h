/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * File: TestODrive_types.h
 *
 * Code generated for Simulink model 'TestODrive'.
 *
 * Model version                  : 6.6
 * Simulink Coder version         : 9.7 (R2022a) 13-Nov-2021
 * C/C++ source code generated on : Mon Feb 13 12:19:25 2023
 *
 * Target selection: ert_linux.tlc
 * Embedded hardware selection: Intel->x86-64 (Windows64)
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_TestODrive_types_h_
#define RTW_HEADER_TestODrive_types_h_
#include "rtwtypes.h"

/* Model Code Variants */
#ifndef struct_tag_d9lchvj4jttRkNL3GSHoe
#define struct_tag_d9lchvj4jttRkNL3GSHoe

struct tag_d9lchvj4jttRkNL3GSHoe
{
  char_T f1;
};

#endif                                 /* struct_tag_d9lchvj4jttRkNL3GSHoe */

#ifndef typedef_cell_TestODrive_T
#define typedef_cell_TestODrive_T

typedef struct tag_d9lchvj4jttRkNL3GSHoe cell_TestODrive_T;

#endif                                 /* typedef_cell_TestODrive_T */

#ifndef struct_tag_SZXGEirNzX0ToQb5h7L8uC
#define struct_tag_SZXGEirNzX0ToQb5h7L8uC

struct tag_SZXGEirNzX0ToQb5h7L8uC
{
  char_T f1[26];
  char_T f2[26];
  char_T f3[39];
  char_T f4[11];
  char_T f5[12];
};

#endif                                 /* struct_tag_SZXGEirNzX0ToQb5h7L8uC */

#ifndef typedef_b_cell_TestODrive_T
#define typedef_b_cell_TestODrive_T

typedef struct tag_SZXGEirNzX0ToQb5h7L8uC b_cell_TestODrive_T;

#endif                                 /* typedef_b_cell_TestODrive_T */

#ifndef struct_tag_5Tkn58SWARj845VWoRmDJB
#define struct_tag_5Tkn58SWARj845VWoRmDJB

struct tag_5Tkn58SWARj845VWoRmDJB
{
  boolean_T matlabCodegenIsDeleted;
  int32_T isInitialized;
  boolean_T isSetupComplete;
  cell_TestODrive_T inputParameters;
  b_cell_TestODrive_T outputParameters;
  real_T inputMultiplier[20];
  real_T outputMultiplier[20];
  int32_T portFilePointer;
};

#endif                                 /* struct_tag_5Tkn58SWARj845VWoRmDJB */

#ifndef typedef_ODrive_TestODrive_T
#define typedef_ODrive_TestODrive_T

typedef struct tag_5Tkn58SWARj845VWoRmDJB ODrive_TestODrive_T;

#endif                                 /* typedef_ODrive_TestODrive_T */

#ifndef struct_tag_qZCSYzgproVuhuNtWU5gYB
#define struct_tag_qZCSYzgproVuhuNtWU5gYB

struct tag_qZCSYzgproVuhuNtWU5gYB
{
  char_T f1[25];
};

#endif                                 /* struct_tag_qZCSYzgproVuhuNtWU5gYB */

#ifndef typedef_g_cell_TestODrive_T
#define typedef_g_cell_TestODrive_T

typedef struct tag_qZCSYzgproVuhuNtWU5gYB g_cell_TestODrive_T;

#endif                                 /* typedef_g_cell_TestODrive_T */

#ifndef struct_tag_pH0LOtyHHHvrXE1tk1k8NH
#define struct_tag_pH0LOtyHHHvrXE1tk1k8NH

struct tag_pH0LOtyHHHvrXE1tk1k8NH
{
  char_T f1[31];
  char_T f2[33];
  char_T f3[27];
  char_T f4[18];
  char_T f5[15];
};

#endif                                 /* struct_tag_pH0LOtyHHHvrXE1tk1k8NH */

#ifndef typedef_h_cell_TestODrive_T
#define typedef_h_cell_TestODrive_T

typedef struct tag_pH0LOtyHHHvrXE1tk1k8NH h_cell_TestODrive_T;

#endif                                 /* typedef_h_cell_TestODrive_T */

/* Parameters (default storage) */
typedef struct P_TestODrive_T_ P_TestODrive_T;

/* Forward declaration for rtModel */
typedef struct tag_RTM_TestODrive_T RT_MODEL_TestODrive_T;

#endif                                 /* RTW_HEADER_TestODrive_types_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
