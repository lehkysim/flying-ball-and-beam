/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * File: TestODrive.c
 *
 * Code generated for Simulink model 'TestODrive'.
 *
 * Model version                  : 6.6
 * Simulink Coder version         : 9.7 (R2022a) 13-Nov-2021
 * C/C++ source code generated on : Mon Feb 13 12:19:25 2023
 *
 * Target selection: ert_linux.tlc
 * Embedded hardware selection: Intel->x86-64 (Windows64)
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "TestODrive.h"
#include "TestODrive_types.h"
#include "rtwtypes.h"
#include "TestODrive_private.h"
#include "TestODrive_dt.h"

/* Block signals (default storage) */
B_TestODrive_T TestODrive_B;

/* Block states (default storage) */
DW_TestODrive_T TestODrive_DW;

/* Real-time model */
static RT_MODEL_TestODrive_T TestODrive_M_;
RT_MODEL_TestODrive_T *const TestODrive_M = &TestODrive_M_;

/* Forward declaration for local functions */
static void TestODriv_ODrive_generateInputs(g_cell_TestODrive_T *names,
  cell_TestODrive_T *parameters, real_T multipliers[20]);
static void TestODri_ODrive_generateOutputs(h_cell_TestODrive_T *names,
  b_cell_TestODrive_T *parameters, real_T multipliers[20]);
static void TestODrive_ODrive_setupImpl(ODrive_TestODrive_T *obj);

/* function [names, parameters, multipliers, count] = generateInputs(obj) */
static void TestODriv_ODrive_generateInputs(g_cell_TestODrive_T *names,
  cell_TestODrive_T *parameters, real_T multipliers[20])
{
  int32_T i;
  static const char_T tmp[17] = { 'A', 'x', 'i', 's', ' ', '0', ' ', 'R', 'e',
    'f', 'e', 'r', 'e', 'n', 'c', 'e', ' ' };

  static const char_T tmp_0[8] = { 'p', 'o', 's', 'i', 't', 'i', 'o', 'n' };

  /* 'ODrive:316' count = 1; */
  /* 'ODrive:317' parameters = cell(1, obj.MaxInputParameters); */
  /* 'ODrive:318' names = cell(1, obj.MaxInputParameters); */
  /* 'ODrive:319' multipliers = ones(1, obj.MaxInputParameters); */
  for (i = 0; i < 20; i++) {
    multipliers[i] = 1.0;
  }

  /* 'ODrive:320' if obj.EnableAxis0 */
  /* 'ODrive:321' names{count} = ['Axis 0 Reference ', lower(obj.ControlMode0)]; */
  for (i = 0; i < 17; i++) {
    names->f1[i] = tmp[i];
  }

  for (i = 0; i < 8; i++) {
    names->f1[i + 17] = tmp_0[i];
  }

  /* 'ODrive:322' parameters{count} = lower(obj.ControlMode0(1)); */
  parameters->f1 = 'p';

  /* 'ODrive:323' if parameters{count} == 'v' || parameters{count} == 'p' */
  /* 'ODrive:324' multipliers(count) = 1/(2*pi); */
  multipliers[0] = 0.15915494309189535;

  /* 'ODrive:326' count=count+1; */
  /* 'ODrive:328' if obj.EnableAxis1 */
  /* 'ODrive:337' if ~isempty(obj.Inputs) */
  /* 'ODrive:345' if (count-1) > obj.MaxInputParameters */
  /* 'ODrive:349' assert(length(names) == length(parameters)); */
  /* 'ODrive:351' for ind = count:obj.MaxInputParameters */
  /* 'ODrive:352' names{ind} = ''; */
  /* 'ODrive:353' parameters{ind} = ''; */
  /* 'ODrive:352' names{ind} = ''; */
  /* 'ODrive:353' parameters{ind} = ''; */
  /* 'ODrive:352' names{ind} = ''; */
  /* 'ODrive:353' parameters{ind} = ''; */
  /* 'ODrive:352' names{ind} = ''; */
  /* 'ODrive:353' parameters{ind} = ''; */
  /* 'ODrive:352' names{ind} = ''; */
  /* 'ODrive:353' parameters{ind} = ''; */
  /* 'ODrive:352' names{ind} = ''; */
  /* 'ODrive:353' parameters{ind} = ''; */
  /* 'ODrive:352' names{ind} = ''; */
  /* 'ODrive:353' parameters{ind} = ''; */
  /* 'ODrive:352' names{ind} = ''; */
  /* 'ODrive:353' parameters{ind} = ''; */
  /* 'ODrive:352' names{ind} = ''; */
  /* 'ODrive:353' parameters{ind} = ''; */
  /* 'ODrive:352' names{ind} = ''; */
  /* 'ODrive:353' parameters{ind} = ''; */
  /* 'ODrive:352' names{ind} = ''; */
  /* 'ODrive:353' parameters{ind} = ''; */
  /* 'ODrive:352' names{ind} = ''; */
  /* 'ODrive:353' parameters{ind} = ''; */
  /* 'ODrive:352' names{ind} = ''; */
  /* 'ODrive:353' parameters{ind} = ''; */
  /* 'ODrive:352' names{ind} = ''; */
  /* 'ODrive:353' parameters{ind} = ''; */
  /* 'ODrive:352' names{ind} = ''; */
  /* 'ODrive:353' parameters{ind} = ''; */
  /* 'ODrive:352' names{ind} = ''; */
  /* 'ODrive:353' parameters{ind} = ''; */
  /* 'ODrive:352' names{ind} = ''; */
  /* 'ODrive:353' parameters{ind} = ''; */
  /* 'ODrive:352' names{ind} = ''; */
  /* 'ODrive:353' parameters{ind} = ''; */
  /* 'ODrive:352' names{ind} = ''; */
  /* 'ODrive:353' parameters{ind} = ''; */
  /* 'ODrive:355' count = count - 1; */
}

/* function [names, parameters, multipliers, count] = generateOutputs(obj) */
static void TestODri_ODrive_generateOutputs(h_cell_TestODrive_T *names,
  b_cell_TestODrive_T *parameters, real_T multipliers[20])
{
  int32_T i;
  static const char_T tmp[26] = { 'a', 'x', 'i', 's', '0', '.', 'e', 'n', 'c',
    'o', 'd', 'e', 'r', '.', 'p', 'o', 's', '_', 'e', 's', 't', 'i', 'm', 'a',
    't', 'e' };

  static const char_T tmp_0[31] = { 'A', 'x', 'i', 's', ' ', '0', ' ', 'E', 's',
    't', 'i', 'm', 'a', 't', 'e', 'd', ' ', 'p', 'o', 's', 'i', 't', 'i', 'o',
    'n', ' ', '[', 'r', 'a', 'd', ']' };

  static const char_T tmp_1[26] = { 'a', 'x', 'i', 's', '0', '.', 'e', 'n', 'c',
    'o', 'd', 'e', 'r', '.', 'v', 'e', 'l', '_', 'e', 's', 't', 'i', 'm', 'a',
    't', 'e' };

  static const char_T tmp_2[33] = { 'A', 'x', 'i', 's', ' ', '0', ' ', 'E', 's',
    't', 'i', 'm', 'a', 't', 'e', 'd', ' ', 'v', 'e', 'l', 'o', 'c', 'i', 't',
    'y', ' ', '[', 'r', 'a', 'd', '/', 's', ']' };

  static const char_T tmp_3[39] = { 'a', 'x', 'i', 's', '0', '.', 'm', 'o', 't',
    'o', 'r', '.', 'c', 'u', 'r', 'r', 'e', 'n', 't', '_', 'c', 'o', 'n', 't',
    'r', 'o', 'l', '.', 'I', 'q', '_', 'm', 'e', 'a', 's', 'u', 'r', 'e', 'd' };

  static const char_T tmp_4[27] = { 'A', 'x', 'i', 's', ' ', '0', ' ', 'M', 'e',
    'a', 's', 'u', 'r', 'e', 'd', ' ', 'c', 'u', 'r', 'r', 'e', 'n', 't', ' ',
    '[', 'A', ']' };

  static const char_T tmp_5[11] = { 'a', 'x', 'i', 's', '0', '.', 'e', 'r', 'r',
    'o', 'r' };

  static const char_T tmp_6[18] = { 'A', 'x', 'i', 's', ' ', '0', ' ', 'E', 'r',
    'r', 'o', 'r', ' ', 's', 't', 'a', 't', 'e' };

  static const char_T tmp_7[12] = { 'v', 'b', 'u', 's', '_', 'v', 'o', 'l', 't',
    'a', 'g', 'e' };

  static const char_T tmp_8[15] = { 'B', 'u', 's', ' ', 'v', 'o', 'l', 't', 'a',
    'g', 'e', ' ', '[', 'V', ']' };

  /* 'ODrive:359' count = 1; */
  /* 'ODrive:360' parameters = cell(1,obj.MaxOutputParameters); */
  /* 'ODrive:361' names = cell(1,obj.MaxOutputParameters); */
  /* 'ODrive:362' multipliers = ones(1, obj.MaxOutputParameters); */
  for (i = 0; i < 20; i++) {
    multipliers[i] = 1.0;
  }

  /* 'ODrive:363' if obj.EnableAxis0 */
  /* 'ODrive:364' if obj.EnablePosition0Output */
  /* 'ODrive:365' parameters{count} = 'axis0.encoder.pos_estimate'; */
  for (i = 0; i < 26; i++) {
    parameters->f1[i] = tmp[i];
  }

  /* 'ODrive:366' names{count} = 'Axis 0 Estimated position [rad]'; */
  for (i = 0; i < 31; i++) {
    names->f1[i] = tmp_0[i];
  }

  /* 'ODrive:367' multipliers(count) = 2*pi; */
  multipliers[0] = 6.2831853071795862;

  /* 'ODrive:368' count=count+1; */
  /* 'ODrive:371' if obj.EnableVelocity0Output */
  /* 'ODrive:372' parameters{count} = 'axis0.encoder.vel_estimate'; */
  for (i = 0; i < 26; i++) {
    parameters->f2[i] = tmp_1[i];
  }

  /* 'ODrive:373' names{count} = 'Axis 0 Estimated velocity [rad/s]'; */
  for (i = 0; i < 33; i++) {
    names->f2[i] = tmp_2[i];
  }

  /* 'ODrive:374' multipliers(count) = 2*pi; */
  multipliers[1] = 6.2831853071795862;

  /* 'ODrive:375' count=count+1; */
  /* 'ODrive:378' if obj.EnableCurrent0Output */
  /* 'ODrive:379' parameters{count} = 'axis0.motor.current_control.Iq_measured'; */
  for (i = 0; i < 39; i++) {
    parameters->f3[i] = tmp_3[i];
  }

  /* 'ODrive:380' names{count} = 'Axis 0 Measured current [A]'; */
  for (i = 0; i < 27; i++) {
    names->f3[i] = tmp_4[i];
  }

  /* 'ODrive:381' count=count+1; */
  /* 'ODrive:384' if obj.EnableError0Output */
  /* 'ODrive:385' parameters{count} = 'axis0.error'; */
  for (i = 0; i < 11; i++) {
    parameters->f4[i] = tmp_5[i];
  }

  /* 'ODrive:386' names{count} = 'Axis 0 Error state'; */
  for (i = 0; i < 18; i++) {
    names->f4[i] = tmp_6[i];
  }

  /* 'ODrive:387' count=count+1; */
  /* 'ODrive:390' if obj.EnableAxis1 */
  /* 'ODrive:417' if obj.EnableVbusOutput */
  /* 'ODrive:418' parameters{count} = 'vbus_voltage'; */
  for (i = 0; i < 12; i++) {
    parameters->f5[i] = tmp_7[i];
  }

  /* 'ODrive:419' names{count} = 'Bus voltage [V]'; */
  for (i = 0; i < 15; i++) {
    names->f5[i] = tmp_8[i];
  }

  /* 'ODrive:420' count=count+1; */
  /* 'ODrive:423' if ~isempty(obj.Outputs) */
  /* 'ODrive:431' if (count-1) > obj.MaxOutputParameters */
  /* 'ODrive:435' for ind = count:obj.MaxOutputParameters */
  /* 'ODrive:436' names{ind} = ''; */
  /* 'ODrive:437' parameters{ind} = ''; */
  /* 'ODrive:436' names{ind} = ''; */
  /* 'ODrive:437' parameters{ind} = ''; */
  /* 'ODrive:436' names{ind} = ''; */
  /* 'ODrive:437' parameters{ind} = ''; */
  /* 'ODrive:436' names{ind} = ''; */
  /* 'ODrive:437' parameters{ind} = ''; */
  /* 'ODrive:436' names{ind} = ''; */
  /* 'ODrive:437' parameters{ind} = ''; */
  /* 'ODrive:436' names{ind} = ''; */
  /* 'ODrive:437' parameters{ind} = ''; */
  /* 'ODrive:436' names{ind} = ''; */
  /* 'ODrive:437' parameters{ind} = ''; */
  /* 'ODrive:436' names{ind} = ''; */
  /* 'ODrive:437' parameters{ind} = ''; */
  /* 'ODrive:436' names{ind} = ''; */
  /* 'ODrive:437' parameters{ind} = ''; */
  /* 'ODrive:436' names{ind} = ''; */
  /* 'ODrive:437' parameters{ind} = ''; */
  /* 'ODrive:436' names{ind} = ''; */
  /* 'ODrive:437' parameters{ind} = ''; */
  /* 'ODrive:436' names{ind} = ''; */
  /* 'ODrive:437' parameters{ind} = ''; */
  /* 'ODrive:436' names{ind} = ''; */
  /* 'ODrive:437' parameters{ind} = ''; */
  /* 'ODrive:436' names{ind} = ''; */
  /* 'ODrive:437' parameters{ind} = ''; */
  /* 'ODrive:436' names{ind} = ''; */
  /* 'ODrive:437' parameters{ind} = ''; */
  /* 'ODrive:439' count = count - 1; */
}

/* function setupImpl(obj) */
static void TestODrive_ODrive_setupImpl(ODrive_TestODrive_T *obj)
{
  g_cell_TestODrive_T a__1;
  h_cell_TestODrive_T a__3;
  int32_T i_0;
  char_T u[48];
  char_T t[42];
  char_T p[39];
  char_T w[37];
  char_T q[36];
  char_T m[34];
  char_T k[31];
  char_T n[26];
  char_T i[23];
  char_T v[22];
  char_T h[20];
  char_T g[18];
  char_T f[13];
  char_T j[12];
  boolean_T encoder0_ready;
  boolean_T motor0_calibrated;
  static const char_T tmp[13] = "/dev/ttyACM0";
  static const char_T tmp_0[18] = "axis0.motor.error";
  static const char_T tmp_1[20] = "axis0.encoder.error";
  static const char_T tmp_2[23] = "axis0.controller.error";
  static const char_T tmp_3[12] = "axis0.error";
  static const char_T tmp_4[31] = "axis0.encoder.config.use_index";
  static const char_T tmp_5[31] = "axis0.motor.config.current_lim";
  static const char_T tmp_6[34] = "axis0.controller.config.vel_limit";
  static const char_T tmp_7[26] = "axis0.motor.is_calibrated";
  static const char_T tmp_8[23] = "axis0.encoder.is_ready";
  static const char_T tmp_9[39] = "axis0.config.startup_motor_calibration";
  static const char_T tmp_a[42] = "axis0.config.startup_encoder_index_search";
  static const char_T tmp_b[36] = "axis0.encoder.config.pre_calibrated";
  static const char_T tmp_c[48] =
    "axis0.config.startup_encoder_offset_calibration";
  static const char_T tmp_d[22] = "axis0.requested_state";
  static const char_T tmp_e[37] = "axis0.controller.config.control_mode";

  /* 'ODrive:103' obj.portFilePointer = int32(0); */
  obj->portFilePointer = 0;

  /* 'ODrive:104' if isempty(coder.target) */
  /* 'ODrive:106' else */
  /* 'ODrive:107' [~, obj.inputParameters, obj.inputMultiplier, ~] = obj.generateInputs(); */
  TestODriv_ODrive_generateInputs(&a__1, &obj->inputParameters,
    obj->inputMultiplier);

  /* 'ODrive:108' [~, obj.outputParameters, obj.outputMultiplier, ~] = obj.generateOutputs(); */
  TestODri_ODrive_generateOutputs(&a__3, &obj->outputParameters,
    obj->outputMultiplier);

  /* 'ODrive:109' coder.cinclude('odrive.h'); */
  /*  Call C-function implementing device initialization */
  /* 'ODrive:111' obj.portFilePointer = coder.ceval('odrive_open_port', cstring(obj.Port), int32(obj.Baudrate)); */
  for (i_0 = 0; i_0 < 13; i_0++) {
    f[i_0] = tmp[i_0];
  }

  obj->portFilePointer = odrive_open_port(&f[0], 115200);

  /* 'ODrive:113' motor0_calibrated = false; */
  /* 'ODrive:114' motor1_calibrated = false; */
  /* 'ODrive:115' encoder0_ready = false; */
  /* 'ODrive:116' encoder1_ready = false; */
  /* 'ODrive:118' should_store_calibration = false; */
  /* 'ODrive:120' if obj.EnableAxis0 */
  /* 'ODrive:121' if obj.ResetErrors0 */
  /*  Reset all error codes */
  /* 'ODrive:123' coder.ceval('odrive_write_int', obj.portFilePointer, cstring('axis0.motor.error'), int32(0)); */
  for (i_0 = 0; i_0 < 18; i_0++) {
    g[i_0] = tmp_0[i_0];
  }

  odrive_write_int(obj->portFilePointer, &g[0], 0);

  /* 'ODrive:124' coder.ceval('odrive_write_int', obj.portFilePointer, cstring('axis0.encoder.error'), int32(0)); */
  for (i_0 = 0; i_0 < 20; i_0++) {
    h[i_0] = tmp_1[i_0];
  }

  odrive_write_int(obj->portFilePointer, &h[0], 0);

  /* 'ODrive:125' coder.ceval('odrive_write_int', obj.portFilePointer, cstring('axis0.controller.error'), int32(0)); */
  for (i_0 = 0; i_0 < 23; i_0++) {
    i[i_0] = tmp_2[i_0];
  }

  odrive_write_int(obj->portFilePointer, &i[0], 0);

  /* 'ODrive:126' coder.ceval('odrive_write_int', obj.portFilePointer, cstring('axis0.error'), int32(0)); */
  for (i_0 = 0; i_0 < 12; i_0++) {
    j[i_0] = tmp_3[i_0];
  }

  odrive_write_int(obj->portFilePointer, &j[0], 0);

  /* 'ODrive:129' coder.ceval('odrive_write_int', obj.portFilePointer, cstring('axis0.encoder.config.use_index'), int32(obj.UseIndex0)); */
  for (i_0 = 0; i_0 < 31; i_0++) {
    k[i_0] = tmp_4[i_0];
  }

  odrive_write_int(obj->portFilePointer, &k[0], 1);

  /* 'ODrive:130' coder.ceval('odrive_write_float', obj.portFilePointer, cstring('axis0.motor.config.current_lim'), obj.CurrentLimit0); */
  for (i_0 = 0; i_0 < 31; i_0++) {
    k[i_0] = tmp_5[i_0];
  }

  odrive_write_float(obj->portFilePointer, &k[0], 10.0);

  /* 'ODrive:131' vel_limit = obj.VelocityLimit0/(2*pi); */
  /* 'ODrive:132' coder.ceval('odrive_write_float', obj.portFilePointer, cstring('axis0.controller.config.vel_limit'), vel_limit); */
  for (i_0 = 0; i_0 < 34; i_0++) {
    m[i_0] = tmp_6[i_0];
  }

  odrive_write_float(obj->portFilePointer, &m[0], 10.0);

  /* 'ODrive:133' motor0_calibrated = coder.ceval('odrive_read_int', obj.portFilePointer, cstring('axis0.motor.is_calibrated')); */
  for (i_0 = 0; i_0 < 26; i_0++) {
    n[i_0] = tmp_7[i_0];
  }

  motor0_calibrated = odrive_read_int(obj->portFilePointer, &n[0]);

  /* 'ODrive:134' encoder0_ready = coder.ceval('odrive_read_int', obj.portFilePointer, cstring('axis0.encoder.is_ready')); */
  for (i_0 = 0; i_0 < 23; i_0++) {
    i[i_0] = tmp_8[i_0];
  }

  encoder0_ready = odrive_read_int(obj->portFilePointer, &i[0]);

  /* 'ODrive:137' if obj.EnableAxis1 */
  /* 'ODrive:153' if(strcmp(obj.Autocalibration,'Disabled (fail if not calibrated)')) */
  /* 'ODrive:162' if startsWith(obj.Autocalibration,'Autocalibrate') */
  /* 'ODrive:163' if obj.EnableAxis0 */
  /* 'ODrive:164' coder.ceval('odrive_write_int', obj.portFilePointer, cstring('axis0.config.startup_motor_calibration'), ~motor0_calibrated); */
  for (i_0 = 0; i_0 < 39; i_0++) {
    p[i_0] = tmp_9[i_0];
  }

  odrive_write_int(obj->portFilePointer, &p[0], !motor0_calibrated);

  /* 'ODrive:165' if ~motor0_calibrated */
  /* 'ODrive:169' if ~encoder0_ready */
  if (!encoder0_ready) {
    /* 'ODrive:170' if obj.UseIndex0 */
    /* 'ODrive:171' encoder0_precalibrated = false; */
    /* 'ODrive:172' encoder0_precalibrated = coder.ceval('odrive_read_int', obj.portFilePointer, cstring('axis0.encoder.config.pre_calibrated')); */
    for (i_0 = 0; i_0 < 36; i_0++) {
      q[i_0] = tmp_b[i_0];
    }

    motor0_calibrated = odrive_read_int(obj->portFilePointer, &q[0]);

    /* 'ODrive:174' coder.ceval('odrive_write_int', obj.portFilePointer, cstring('axis0.config.startup_encoder_index_search'), true); */
    for (i_0 = 0; i_0 < 42; i_0++) {
      t[i_0] = tmp_a[i_0];
    }

    odrive_write_int(obj->portFilePointer, &t[0], true);

    /* 'ODrive:175' coder.ceval('odrive_write_int', obj.portFilePointer, cstring('axis0.config.startup_encoder_offset_calibration'), ~encoder0_precalibrated); */
    for (i_0 = 0; i_0 < 48; i_0++) {
      u[i_0] = tmp_c[i_0];
    }

    odrive_write_int(obj->portFilePointer, &u[0], !motor0_calibrated);

    /* 'ODrive:176' if ~encoder0_precalibrated */
  } else {
    /* 'ODrive:184' else */
    /* 'ODrive:185' coder.ceval('odrive_write_int', obj.portFilePointer, cstring('axis0.config.startup_encoder_index_search'), false); */
    for (i_0 = 0; i_0 < 42; i_0++) {
      t[i_0] = tmp_a[i_0];
    }

    odrive_write_int(obj->portFilePointer, &t[0], false);

    /* 'ODrive:186' coder.ceval('odrive_write_int', obj.portFilePointer, cstring('axis0.config.startup_encoder_offset_calibration'), false); */
    for (i_0 = 0; i_0 < 48; i_0++) {
      u[i_0] = tmp_c[i_0];
    }

    odrive_write_int(obj->portFilePointer, &u[0], false);
  }

  /* 'ODrive:189' coder.ceval('odrive_write_int', obj.portFilePointer, cstring('axis0.requested_state'), int32(2)); */
  for (i_0 = 0; i_0 < 22; i_0++) {
    v[i_0] = tmp_d[i_0];
  }

  odrive_write_int(obj->portFilePointer, &v[0], 2);

  /* 'ODrive:192' if obj.EnableAxis1 */
  /* 'ODrive:220' if obj.EnableAxis0 */
  /* 'ODrive:221' coder.ceval('odrive_wait_for_state', obj.portFilePointer, int32(0), int32(1), int32(100000), int32(0)); */
  odrive_wait_for_state(obj->portFilePointer, 0, 1, 100000, 0);

  /* 'ODrive:224' if obj.EnableAxis1 */
  /* 'ODrive:229' if strcmp(obj.Autocalibration, 'Autocalibrate and store') && should_store_calibration */
  /* 'ODrive:241' if obj.EnableAxis0 */
  /* 'ODrive:242' switch(obj.ControlMode0) */
  /* 'ODrive:243' case 'Position' */
  /* 'ODrive:244' coder.ceval('odrive_write_int', obj.portFilePointer, cstring('axis0.controller.config.control_mode'), int32(3)); */
  for (i_0 = 0; i_0 < 37; i_0++) {
    w[i_0] = tmp_e[i_0];
  }

  odrive_write_int(obj->portFilePointer, &w[0], 3);

  /* 'ODrive:250' coder.ceval('odrive_write_int', obj.portFilePointer, cstring('axis0.requested_state'), int32(8)); */
  for (i_0 = 0; i_0 < 22; i_0++) {
    v[i_0] = tmp_d[i_0];
  }

  odrive_write_int(obj->portFilePointer, &v[0], 8);

  /* 'ODrive:253' if obj.EnableAxis1 */
}

/* Model step function */
void TestODrive_step(void)
{
  real_T value;
  int32_T i;
  char_T d[40];
  char_T b[27];
  char_T f[13];
  char_T e[12];
  uint8_T tmp;

  /* DiscretePulseGenerator: '<Root>/Pulse Generator' */
  TestODrive_B.pulse = (TestODrive_DW.clockTickCounter <
                        TestODrive_P.PulseGenerator_Duty) &&
    (TestODrive_DW.clockTickCounter >= 0) ? TestODrive_P.PulseGenerator_Amp :
    0.0;

  /* DiscretePulseGenerator: '<Root>/Pulse Generator' */
  if (TestODrive_DW.clockTickCounter >= TestODrive_P.PulseGenerator_Period - 1.0)
  {
    TestODrive_DW.clockTickCounter = 0;
  } else {
    TestODrive_DW.clockTickCounter++;
  }

  /* MATLABSystem: '<Root>/MATLAB System' */
  /*         %% Define input properties */
  /* 'ODrive:448' [~, ~, ~, num] = obj.generateInputs(); */
  /* 'ODrive:452' [~, ~, ~, num] = obj.generateOutputs(); */
  /* 'ODrive:268' if isempty(coder.target) */
  /* 'ODrive:273' else */
  /* 'ODrive:274' startTime = 0; */
  /* 'ODrive:275' endTime = 0; */
  /* 'ODrive:277' if obj.EnableTiming */
  /* 'ODrive:280' for ind = 1:(nargin-1) */
  /* 'ODrive:281' value = varargin{ind}*obj.inputMultiplier(ind); */
  /* 'ODrive:282' if length(obj.inputParameters{ind}) == 1 */
  /* 'ODrive:283' coder.ceval('odrive_quick_write', obj.portFilePointer, int8(obj.inputParameters{ind}(1)), int32(0), value); */
  tmp = (uint8_T)TestODrive_DW.obj.inputParameters.f1;
  if ((uint8_T)TestODrive_DW.obj.inputParameters.f1 > 127) {
    tmp = 127U;
  }

  odrive_quick_write(TestODrive_DW.obj.portFilePointer, (int8_T)tmp, 0,
                     TestODrive_B.pulse * TestODrive_DW.obj.inputMultiplier[0]);

  /* 'ODrive:288' for ind = 1:nargout */
  /* 'ODrive:289' value = 0; */
  /* 'ODrive:290' value = coder.ceval('odrive_read_float', obj.portFilePointer, cstring(obj.outputParameters{ind})); */
  /* 'ODrive:614' output = [input 0]; */
  for (i = 0; i < 26; i++) {
    b[i] = TestODrive_DW.obj.outputParameters.f1[i];
  }

  b[26] = '\x00';
  value = odrive_read_float(TestODrive_DW.obj.portFilePointer, &b[0]);

  /* MATLABSystem: '<Root>/MATLAB System' */
  /* 'ODrive:291' value = value * obj.outputMultiplier(ind); */
  TestODrive_B.ang = value * TestODrive_DW.obj.outputMultiplier[0];

  /* MATLABSystem: '<Root>/MATLAB System' */
  /* 'ODrive:292' varargout{ind} = value; */
  /* 'ODrive:289' value = 0; */
  /* 'ODrive:290' value = coder.ceval('odrive_read_float', obj.portFilePointer, cstring(obj.outputParameters{ind})); */
  /* 'ODrive:614' output = [input 0]; */
  for (i = 0; i < 26; i++) {
    b[i] = TestODrive_DW.obj.outputParameters.f2[i];
  }

  b[26] = '\x00';
  value = odrive_read_float(TestODrive_DW.obj.portFilePointer, &b[0]);

  /* MATLABSystem: '<Root>/MATLAB System' */
  /* 'ODrive:291' value = value * obj.outputMultiplier(ind); */
  TestODrive_B.vel = value * TestODrive_DW.obj.outputMultiplier[1];

  /* MATLABSystem: '<Root>/MATLAB System' */
  /* 'ODrive:292' varargout{ind} = value; */
  /* 'ODrive:289' value = 0; */
  /* 'ODrive:290' value = coder.ceval('odrive_read_float', obj.portFilePointer, cstring(obj.outputParameters{ind})); */
  /* 'ODrive:614' output = [input 0]; */
  for (i = 0; i < 39; i++) {
    d[i] = TestODrive_DW.obj.outputParameters.f3[i];
  }

  d[39] = '\x00';
  value = odrive_read_float(TestODrive_DW.obj.portFilePointer, &d[0]);

  /* MATLABSystem: '<Root>/MATLAB System' */
  /* 'ODrive:291' value = value * obj.outputMultiplier(ind); */
  TestODrive_B.current = value * TestODrive_DW.obj.outputMultiplier[2];

  /* MATLABSystem: '<Root>/MATLAB System' */
  /* 'ODrive:292' varargout{ind} = value; */
  /* 'ODrive:289' value = 0; */
  /* 'ODrive:290' value = coder.ceval('odrive_read_float', obj.portFilePointer, cstring(obj.outputParameters{ind})); */
  /* 'ODrive:614' output = [input 0]; */
  for (i = 0; i < 11; i++) {
    e[i] = TestODrive_DW.obj.outputParameters.f4[i];
  }

  e[11] = '\x00';
  value = odrive_read_float(TestODrive_DW.obj.portFilePointer, &e[0]);

  /* MATLABSystem: '<Root>/MATLAB System' */
  /* 'ODrive:291' value = value * obj.outputMultiplier(ind); */
  TestODrive_B.error = value * TestODrive_DW.obj.outputMultiplier[3];

  /* MATLABSystem: '<Root>/MATLAB System' */
  /* 'ODrive:292' varargout{ind} = value; */
  /* 'ODrive:289' value = 0; */
  /* 'ODrive:290' value = coder.ceval('odrive_read_float', obj.portFilePointer, cstring(obj.outputParameters{ind})); */
  /* 'ODrive:614' output = [input 0]; */
  for (i = 0; i < 12; i++) {
    f[i] = TestODrive_DW.obj.outputParameters.f5[i];
  }

  f[12] = '\x00';
  value = odrive_read_float(TestODrive_DW.obj.portFilePointer, &f[0]);

  /* MATLABSystem: '<Root>/MATLAB System' */
  /* 'ODrive:291' value = value * obj.outputMultiplier(ind); */
  /* 'ODrive:292' varargout{ind} = value; */
  /* 'ODrive:294' if obj.EnableTiming */
  TestODrive_B.V = value * TestODrive_DW.obj.outputMultiplier[4];

  /* External mode */
  rtExtModeUploadCheckTrigger(1);

  {                                    /* Sample time: [2.5s, 0.0s] */
    rtExtModeUpload(0, (real_T)TestODrive_M->Timing.taskTime0);
  }

  /* signal main to stop simulation */
  {                                    /* Sample time: [2.5s, 0.0s] */
    if ((rtmGetTFinal(TestODrive_M)!=-1) &&
        !((rtmGetTFinal(TestODrive_M)-TestODrive_M->Timing.taskTime0) >
          TestODrive_M->Timing.taskTime0 * (DBL_EPSILON))) {
      rtmSetErrorStatus(TestODrive_M, "Simulation finished");
    }

    if (rtmGetStopRequested(TestODrive_M)) {
      rtmSetErrorStatus(TestODrive_M, "Simulation finished");
    }
  }

  /* Update absolute time for base rate */
  /* The "clockTick0" counts the number of times the code of this task has
   * been executed. The absolute time is the multiplication of "clockTick0"
   * and "Timing.stepSize0". Size of "clockTick0" ensures timer will not
   * overflow during the application lifespan selected.
   */
  TestODrive_M->Timing.taskTime0 =
    ((time_T)(++TestODrive_M->Timing.clockTick0)) *
    TestODrive_M->Timing.stepSize0;
}

/* Model initialize function */
void TestODrive_initialize(void)
{
  /* Registration code */

  /* initialize real-time model */
  (void) memset((void *)TestODrive_M, 0,
                sizeof(RT_MODEL_TestODrive_T));
  rtmSetTFinal(TestODrive_M, 10.0);
  TestODrive_M->Timing.stepSize0 = 2.5;

  /* External mode info */
  TestODrive_M->Sizes.checksums[0] = (3547888863U);
  TestODrive_M->Sizes.checksums[1] = (3991325455U);
  TestODrive_M->Sizes.checksums[2] = (2945900809U);
  TestODrive_M->Sizes.checksums[3] = (2554526801U);

  {
    static const sysRanDType rtAlwaysEnabled = SUBSYS_RAN_BC_ENABLE;
    static RTWExtModeInfo rt_ExtModeInfo;
    static const sysRanDType *systemRan[2];
    TestODrive_M->extModeInfo = (&rt_ExtModeInfo);
    rteiSetSubSystemActiveVectorAddresses(&rt_ExtModeInfo, systemRan);
    systemRan[0] = &rtAlwaysEnabled;
    systemRan[1] = &rtAlwaysEnabled;
    rteiSetModelMappingInfoPtr(TestODrive_M->extModeInfo,
      &TestODrive_M->SpecialInfo.mappingInfo);
    rteiSetChecksumsPtr(TestODrive_M->extModeInfo, TestODrive_M->Sizes.checksums);
    rteiSetTPtr(TestODrive_M->extModeInfo, rtmGetTPtr(TestODrive_M));
  }

  /* block I/O */
  (void) memset(((void *) &TestODrive_B), 0,
                sizeof(B_TestODrive_T));

  /* states (dwork) */
  (void) memset((void *)&TestODrive_DW, 0,
                sizeof(DW_TestODrive_T));

  /* data type transition information */
  {
    static DataTypeTransInfo dtInfo;
    (void) memset((char_T *) &dtInfo, 0,
                  sizeof(dtInfo));
    TestODrive_M->SpecialInfo.mappingInfo = (&dtInfo);
    dtInfo.numDataTypes = 19;
    dtInfo.dataTypeSizes = &rtDataTypeSizes[0];
    dtInfo.dataTypeNames = &rtDataTypeNames[0];

    /* Block I/O transition table */
    dtInfo.BTransTable = &rtBTransTable;

    /* Parameters transition table */
    dtInfo.PTransTable = &rtPTransTable;
  }

  /* Start for DiscretePulseGenerator: '<Root>/Pulse Generator' */
  TestODrive_DW.clockTickCounter = 0;

  /* Start for MATLABSystem: '<Root>/MATLAB System' */
  /* 'ODrive:1' matlab.System */
  /* 'ODrive:2' coder.ExternalDependency */
  /* 'ODrive:3' matlab.system.mixin.Propagates */
  /* 'ODrive:4' matlab.system.mixin.CustomIcon */
  TestODrive_DW.obj.matlabCodegenIsDeleted = false;
  TestODrive_DW.obj.isSetupComplete = false;
  TestODrive_DW.obj.isInitialized = 1;

  /* 'ODrive:456' flag = true; */
  /*         %% Define input properties */
  /* 'ODrive:448' [~, ~, ~, num] = obj.generateInputs(); */
  /*         %% Define input properties */
  /* 'ODrive:448' [~, ~, ~, num] = obj.generateInputs(); */
  /*         %% Define input properties */
  /* 'ODrive:448' [~, ~, ~, num] = obj.generateInputs(); */
  /* 'ODrive:468' if isempty(coder.target) */
  TestODrive_ODrive_setupImpl(&TestODrive_DW.obj);
  TestODrive_DW.obj.isSetupComplete = true;
}

/* Model terminate function */
void TestODrive_terminate(void)
{
  int32_T i;
  char_T b[22];
  static const char_T tmp[22] = "axis0.requested_state";

  /* Terminate for MATLABSystem: '<Root>/MATLAB System' */
  if (!TestODrive_DW.obj.matlabCodegenIsDeleted) {
    TestODrive_DW.obj.matlabCodegenIsDeleted = true;
    if ((TestODrive_DW.obj.isInitialized == 1) &&
        TestODrive_DW.obj.isSetupComplete) {
      /* 'ODrive:302' if isempty(coder.target) */
      /* 'ODrive:304' else */
      /*  Call C-function implementing device termination */
      /* 'ODrive:306' if obj.EnableAxis0 */
      /* 'ODrive:307' coder.ceval('odrive_write_int', obj.portFilePointer, cstring('axis0.requested_state'), int32(1)); */
      for (i = 0; i < 22; i++) {
        b[i] = tmp[i];
      }

      odrive_write_int(TestODrive_DW.obj.portFilePointer, &b[0], 1);

      /* 'ODrive:309' if obj.EnableAxis1 */
    }
  }

  /* End of Terminate for MATLABSystem: '<Root>/MATLAB System' */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
