# ODrive motor setup
    
1) Install python 3 and pip

2) Install odrivetool (ver. 0.5.4) ```sudo pip3 install --upgrade odrive==0.5.4```

3) Configure ODrive driver, encoder and motor \
\* constants set in the configuration may vary - check your motor and encoder type @ https://bit.ly/2HJUy2p
    ```
    odrv0.axisX.motor.config.current_lim = 10
    odrv0.axisX.controller.config.vel_limit = 10
    odrv0.config.enable_brake_resistor = True
    odrv0.config.brake_resistance = 2
    odrv0.config.dc_max_negative_current = -0.1
    odrv0.axisX.motor.config.pole_pairs = 7
    odrv0.axisX.motor.config.torque_constant = 8.27/270
    odrv0.axisX.motor.config.motor_type = MOTOR_TYPE_HIGH_CURRENT [0]
    odrv0.axisX.encoder.config.cpr = 8192
    odrv0.save_configuration()
    ```
    
    The configuration setup can be found here: https://docs.odriverobotics.com/v/0.5.5/getting-started.html
    
    In case of any configuration errors, refer to https://github.com/odriverobotics/ODrive/blob/master/tools/odrive/enums.py

4) Clone AA4CC SimlulinkOdrive repository to your computer @ https://github.com/aa4cc/SimulinkOdrive and follow the readme file

5) Open *./SimulinkOdrive/Odrive.m* in Matlab and replace **obj.CountsPerRotateX** with number **1** in lines \
    131 / 147 / 324 / 332 / 367 / 374 / 394 / 401
    - This change will allow you to display the motor velocity in rad/s instead of CPR

6) Open *./SimulinkOdrive/TestODrive.slx.m* and lessen *Current lim* to **10 A** in the motor MATLAB System function block \

7) Change *CurrentLimitX* to **10 A** in *ODrive.m* file on lines 68 and 69

8) If working with Raspberry, choose correct RPi port in the motor system function for the Simulink-Raspberry communication

To see live ODrive command line while simulating, type `./TestODrive.elf -tf inf` in the command line in your folder 
