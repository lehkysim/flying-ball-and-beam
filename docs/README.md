# Flying Ball and Beam 

> Raspberry Pi 4 Model B with Armor Fan Case \
> MATLAB with Simulink on **Linux** computer

## Setup

1) **PC**: Clone Flying Ball and Beam repository @ https://gitlab.fel.cvut.cz/lehkysim/flying-ball-and-beam

2) **RPi**: Download and install **32-bit** Raspberry Pi OS to your Raspberry @ https://www.raspberrypi.com/software \
(*picamera* library which is used in raspi-ballpos is **not** supported in 64-bit version)

3) **RPi**: Add Raspberry Armor Fan Case to your Raspberry for extra cooling (highly recommended) 

4) **RPi**: Connect Raspberry via Ethernet to Linux device for SSH communication 
    * Be sure to enable SSH communication on your Raspberry with `raspi-config` command
    * Connect both your device and Raspberry via Ethernet to your local network (recommended), or you can connect Raspberry via Ethernet directly to your device and follow this tutorial for complete setup @ https://www.youtube.com/watch?v=KjghxhS_wcM&t=393s&ab_channel=surhud004 \
    * connect to your Raspberry `ssh pi@rpi_IP`
    
5) **PC**: Set passwordless ssh connection @ https://phoenixnap.com/kb/setup-passwordless-ssh   
    
6) **RPi**: Update Raspberry with `sudo apt update`, `sudo apt upgrade`, `sudo reboot`
    
7) **RPi**: Install python3 and pip

8) **RPi**: Install OpenCV, and Raspberry Shutdown button service
@ https://aa4cc.github.io/flying-ball-in-hoop/buildingInstructions_SW

9) **RPi**: To install and set RaspiBallPos, follow installation instructions in *raspiballpos* README.md with these changes
    * **skip** line 6
    * line 7:
    `cp config_ransac.json config.json` and copy line 56 & 57 from *config.json_sample* to line 21 & 22 in *config.json* to turn on RaspiCam LEDs on startup
    *be sure to choose correct GPIO for RaspiLamp LEDs (change GPIO 11 to 21 in line 21 in *config.json* in our case) and enable it as an output with these commands \
    `raspi-gpio set 21 op` \
    `raspi-gpio set 21 dh`

10) **RPi**: Test if you can launch RaspiCam with `python3 vision.py -vc config.json` in raspi-ballpos folder on Raspberry and change objects' parameters in RaspiBallPos interface @ http://rpi_IP:5001

11) **PC**: Install and set ERT Linux for a MATLAB-Raspberry communication @ https://github.com/aa4cc/ert_linux

12) **PC**: Install Moteus API for Moteus motor @ https://github.com/doloi456/moteusapi_ert
    * For calibration, read README in *moteus* folder in this repository

## Switching on

* **PC**: First, run *load_params.m* script to initialize all necessary variables
* **PC**: Before starting any Simulink file, be sure to add *dependencies* file located in the same folder as .slx file to Matlab path (including their subfolders)
    * If you want to use Moteus motor controlled from Simulink, it is recommended to copy *dependencies* folder from moteus folder in main directory to directory where you create new Simulink file 
* **PC**: In Simulink file, check if Raspberry IP address and name are correct
* **Raspberry**: Launch RaspiCam with `python3 vision.py -vc config.json` and calibrate camera in RaspiBallPos interface
* **PC**: Build and deploy Simulink file
* **PC**: Start simulating

<br/>