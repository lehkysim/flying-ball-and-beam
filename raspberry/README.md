## Raspberry Pi 4 Model B

To get the IP address of the device, you get it either with ```ifconfig``` command in the terminal of the rapsberry or by connecting to the router to which is the Raspberry connected @ ```192.168.0.1/```

* Router name: admin
* Router password: ****n 

The address can be found in the devices table in *DHCP Server -> DHCP List&Binding* tab 

### Raspberry info

* Host name: raspberry 
* User name: pi
* Password: simonrpi
